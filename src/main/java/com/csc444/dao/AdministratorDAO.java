package com.csc444.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.csc444.model.Administrator;
import com.csc444.util.DBUtil;
import com.csc444.utils.MyUtils;


public class AdministratorDAO {
	
	/**
	 * Check email and password
	 * @param email
	 * @param password
	 * @return admin object if match a record. null otherwise
	 * @throws SQLException
	 */
	public static Administrator login(String admin_email, String admin_password) throws SQLException {
		// Admin bean to be returned, default is null
		Administrator admin = null;

		// the SQL query
		String sql = "SELECT * FROM administrator where admin_email=? AND admin_password=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			
			// set parameter in the query
			ps.setString(1, admin_email);
			
			// hash the password first
			String hashedPass = admin_password;
			ps.setString(2, hashedPass);
			
			// execute the query
			resultSet = ps.executeQuery();

			// check if email and password matched any record
			if (resultSet.next()) {
				// get values for this record.
				// skip password because we will not display it anywhere
				// in the web system	
				int admin_id = resultSet.getInt("admin_id");
				String admin_firstname = resultSet.getString("admin_firstname");
				String admin_lastname = resultSet.getString("admin_lastname");
				String admin_nric = resultSet.getString("admin_nric");
				String admin_phoneno = resultSet.getString("admin_phoneno");
				
				// create Administrator bean object based on this record
				admin =  new Administrator(admin_id, admin_firstname, admin_lastname, 
						admin_password, admin_nric, admin_phoneno,  admin_email);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the Administrator bean
		return admin;
	}
	
	/**
	 * Get all admin record from database
	 * 
	 * @return List of Admin objects
	 * @throws SQLException
	 */
	public static List<Administrator> listAllAdministrator() throws SQLException {
		
		// initialize an empty ArrayList
		List<Administrator> listAdministrator = new ArrayList<>();
		
		// the SQL query
		String sql = "SELECT * FROM administrator";
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			statement = connection.createStatement();
			// execute query
			resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				// get values for this record
				int admin_id = resultSet.getInt("admin_id");
				String admin_firstname = resultSet.getString("admin_firstname");
				String admin_lastname = resultSet.getString("admin_lastname");
				String admin_password = resultSet.getString("admin_password");
				String admin_nric = resultSet.getString("admin_nric");
				String admin_phoneno = resultSet.getString("admin_phoneno");
				String admin_email = resultSet.getString("admin_email");
				
				// create Administrator bean object based on this record
				Administrator admin =  new Administrator (admin_id, admin_firstname, admin_lastname, 
						admin_password, admin_nric, admin_phoneno,  admin_email);
				
				// add this Administrator bean object to the ArrayList
				listAdministrator.add(admin);
			}
			
		} // end try
			
		catch (Exception e) {
			e.printStackTrace();
		}
		
		finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (statement != null)
				statement.close();

			if (resultSet != null)
				resultSet.close();
		}
		
		// return the ArrayList with list of Customer object
		return listAdministrator;	
		
	} // end List<Customer>
	
	// -------------------------------------------------------------------------
	/**
	 * Get a single Administrator bean based on the admin_id
	 * 
	 * @param id admin id
	 * @return Administrator bean based on the id. null if not found
	 * @throws SQLException
	 */
	
	public static Administrator getAdministrator(int id) throws SQLException {
		
		// Administrator bean to be returned, default is null
		Administrator admin = null;
		
		// the SQL query
		String sql = "SELECT * FROM administrator where admin_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, id);
			// execute the query
			resultSet = ps.executeQuery();
			
			// check if any record returned
			if (resultSet.next()) {
				// if there is a record, get the data
				int admin_id = resultSet.getInt("admin_id");
				String admin_firstname = resultSet.getString("admin_firstname");
				;
				String admin_lastname = resultSet.getString("admin_lastname");
				;
				String admin_password = resultSet.getString("admin_password");
				;
				String admin_nric = resultSet.getString("admin_nric");
				;
				String admin_phoneno = resultSet.getString("admin_phoneno");
				;
				String admin_email = resultSet.getString("admin_email");
				
				// create Administrator bean object based on this record
				admin =  new Administrator (admin_id, admin_firstname, admin_lastname, 
						admin_password, admin_nric, admin_phoneno,  admin_email);
			}
			
		} // end try
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}
		
		// return the Administrator bean
		return admin;
		
	} // end getAdministrator
	
	// -------------------------------------------------------------------------
	
	/**
	 * Store new Administrator record
	 */	
	public static int storeAdministrator(Administrator admin) throws SQLException {
		// to store Administrator id of this new Administrator
		int adminId = -1;
		
		// the SQL query
		String sql = "insert into Administrator(admin_firstname, admin_lastname, "
				+ "admin_password, admin_nric, admin_phoneno,  admin_email) values (?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			// set parameter in the query
			if (!admin.getAdmin_firstname().equals(""))
				ps.setString(1, admin.getAdmin_firstname());
			else
				ps.setNull(1, java.sql.Types.VARCHAR);
			
			
			if (!admin.getAdmin_lastname().equals(""))
				ps.setString(2, admin.getAdmin_lastname());
			else
				ps.setNull(2, java.sql.Types.VARCHAR);
			
			
			if (!admin.getAdmin_password().equals(""))
				ps.setString(3, admin.getAdmin_password());
			else
				ps.setNull(3, java.sql.Types.VARCHAR);
			
			
			if (!admin.getAdmin_nric().equals(""))
				ps.setString(4, admin.getAdmin_nric());
			else
				ps.setNull(4, java.sql.Types.VARCHAR);
			
			if (!admin.getAdmin_phoneno().equals(""))
				ps.setString(5, admin.getAdmin_phoneno());
			else
				ps.setNull(5, java.sql.Types.VARCHAR);
			
			
			if (!admin.getAdmin_email().equals(""))
				ps.setString(6, admin.getAdmin_email());
			else
				ps.setNull(6, java.sql.Types.VARCHAR);
			
			// execute the query
			ps.executeUpdate();
			
			// get generated admin id
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			adminId = rs.getInt(1);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}
		
		// return admin id of this new administrator to caller. -1 if error
		return adminId;
		
	} // end storeAdministrator
	
	//-------------------------------------------------------------------------
	
	public static Administrator viewAdministrator(int id) {
		// the SQL query
		String sql = "SELECT * FROM administrator where admin_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		Administrator admin = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			// execute query
			resultSet = ps.executeQuery();
			
			if (resultSet.next()) {
				// get values for this record
				int admin_id = resultSet.getInt("admin_id");
				String admin_firstname = resultSet.getString("admin_firstname");
				String admin_lastname = resultSet.getString("admin_lastname");
				String admin_password = resultSet.getString("admin_password");
				String admin_nric = resultSet.getString("admin_nric");
				String admin_phoneno = resultSet.getString("admin_phoneno");
				String admin_email = resultSet.getString("admin_email");
				
				// create Administrator bean object based on this record
				admin =  new Administrator (admin_id, admin_firstname, admin_lastname, 
						admin_password, admin_nric, admin_phoneno,  admin_email);
			}
			
		} // end try
			
		catch (Exception e) {
			e.printStackTrace();
		}
		
		// return the ArrayList with list of Customer object
		return admin;
		
	} // end viewAdministrator
	
	// -------------------------------------------------------------------------
	
	public static boolean deleteAdministrator(int id ) throws SQLException {
		// delete status
		boolean success = false;
		
		// the SQL query
		String sql = "DELETE FROM administrator where admin_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, id);
			// execute the query
			ps.executeUpdate();

			success = true; // successful delete the record

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the status
		return success;
		
	} // end deleteAdministrator
	
	// -------------------------------------------------------------------------
	
	public static boolean updateAdministrator(Administrator admin) throws SQLException {
		
		int numRec = 0; // records updated

		// the SQL query
		String sql = "update Administrator set admin_firstname=?, admin_lastname=?"
				+ ", admin_password=?, admin_nric=?, admin_phoneno=?, admin_email=? where admin_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			// set parameter in the query
			if (!admin.getAdmin_firstname().equals(""))
				ps.setString(1, admin.getAdmin_firstname());
			else
				ps.setNull(1, java.sql.Types.VARCHAR);


			if (!admin.getAdmin_lastname().equals(""))
				ps.setString(2, admin.getAdmin_lastname());
			else
				ps.setNull(2, java.sql.Types.VARCHAR);


			if (!admin.getAdmin_password().equals(""))
				ps.setString(3, admin.getAdmin_password());
			else
				ps.setNull(3, java.sql.Types.VARCHAR);


			if (!admin.getAdmin_nric().equals(""))
				ps.setString(4, admin.getAdmin_nric());
			else
				ps.setNull(4, java.sql.Types.VARCHAR);

			if (!admin.getAdmin_phoneno().equals(""))
				ps.setString(5, admin.getAdmin_phoneno());
			else
				ps.setNull(5, java.sql.Types.VARCHAR);


			if (!admin.getAdmin_email().equals(""))
				ps.setString(6, admin.getAdmin_email());
			else
				ps.setNull(6, java.sql.Types.VARCHAR);

			ps.setInt(7, admin.getAdmin_id());

			// execute the query
			numRec = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		if (numRec > 0)
			return true;
		else
			return false;

	}
		
} // end class AdministratorDAO

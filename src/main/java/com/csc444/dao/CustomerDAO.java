package com.csc444.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.csc444.model.Customer;
import com.csc444.util.DBUtil;
import com.csc444.utils.MyUtils;


public class CustomerDAO {
	
	/**
	 * Check email and password
	 * @param email
	 * @param password
	 * @return customer object if match a record. null otherwise
	 * @throws SQLException
	 */
	public static Customer login(String cust_email, String cust_password) throws SQLException {
		// Customer bean to be returned, default is null
		Customer cust = null;

		// the SQL query
		String sql = "SELECT * FROM customer where cust_email=? AND cust_password=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			
			// set parameter in the query
			ps.setString(1, cust_email);
			
			// hash the password first
			String hashedPass = cust_password;
			ps.setString(2, hashedPass);
			
			// execute the query
			resultSet = ps.executeQuery();

			// check if email and password matched any record
			if (resultSet.next()) {
				// get values for this record.
				// skip password because we will not display it anywhere
				// in the web system	
				int custId = resultSet.getInt("cust_id");
				String custFirstname = resultSet.getString("cust_firstname");
				String custLastname = resultSet.getString("cust_lastname");
				String custPassword = resultSet.getString("cust_password");
				String custNric = resultSet.getString("cust_nric");
				String custAddress = resultSet.getString("cust_address");
				String custPhoneno = resultSet.getString("cust_phoneno");
				String custEmail = resultSet.getString("cust_email");
				
				// create Customer bean object based on this record
				cust =  new Customer (custId, custFirstname, custLastname, 
						custPassword, custNric, custAddress, custPhoneno, custEmail);			
				}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the Customer bean
		return cust;
	}
	
	/**
	 * Get all customer record from database
	 * 
	 * @return List of Customer objects
	 * @throws SQLException
	 */
	public static List<Customer> listAllCustomer() throws SQLException {
		
		// initialize an empty ArrayList
		List<Customer> listCustomer = new ArrayList<>();
		
		// the SQL query
		String sql = "SELECT * FROM customer";
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			statement = connection.createStatement();
			// execute query
			resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				// get values for this record
				int cust_id = resultSet.getInt("cust_id");
				String cust_firstname = resultSet.getString("cust_firstname");
				String cust_lastname = resultSet.getString("cust_lastname");
				String cust_password = resultSet.getString("cust_password");
				String cust_nric = resultSet.getString("cust_nric");
				String cust_address = resultSet.getString("cust_address");
				String cust_phoneno = resultSet.getString("cust_phoneno");
				String cust_email = resultSet.getString("cust_email");
				
				// create Customer bean object based on this record
				Customer cust =  new Customer (cust_id, cust_firstname, cust_lastname, 
						cust_password, cust_nric, cust_address, 
						cust_phoneno,  cust_email);
				
				// add this Customer bean object to the ArrayList
				listCustomer.add(cust);
			}
			
		} // end try
			
		catch (Exception e) {
			e.printStackTrace();
		}
		
		finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (statement != null)
				statement.close();

			if (resultSet != null)
				resultSet.close();
		}
		
		// return the ArrayList with list of Customer object
		return listCustomer;	
		
	} // end List<Customer>
	
	// -------------------------------------------------------------------------
	/**
	 * Get a single Customer bean based on the cust_id
	 * 
	 * @param id cust id
	 * @return Customer bean based on the id. null if not found
	 * @throws SQLException
	 */
	
	public static Customer getCustomer(int id) throws SQLException {
		
		// Customer bean to be returned, default is null
		Customer cust = null;
		
		// the SQL query
		String sql = "SELECT * FROM customer where cust_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, id);
			// execute the query
			resultSet = ps.executeQuery();
			
			// check if any record returned
			if (resultSet.next()) {
				// if there is a record, get the data
				int cust_id = resultSet.getInt("cust_id");
				String cust_firstname = resultSet.getString("cust_firstname");
				;
				String cust_lastname = resultSet.getString("cust_lastname");
				;
				String cust_password = resultSet.getString("cust_password");
				;
				String cust_nric = resultSet.getString("cust_nric");
				;
				String cust_address = resultSet.getString("cust_address");
				;
				String cust_phoneno = resultSet.getString("cust_phoneno");
				;
				String cust_email = resultSet.getString("cust_email");
				
				// create Customer bean object based on this record
				cust =  new Customer (cust_id, cust_firstname, cust_lastname, 
						cust_password, cust_nric, cust_address, 
						cust_phoneno,  cust_email);
			}
			
		} // end try
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}
		
		// return the Customer bean
		return cust;
		
	} // end getCustomer
	
	// -------------------------------------------------------------------------
	
	/**
	 * Store new employee record
	 */	
	public static int storeCustomer(Customer cust) throws SQLException {
		// to store customer id of this new customer
		int custId = -1;
		
		// the SQL query
		String sql = "insert into Customer(cust_firstname, cust_lastname, "
				+ "cust_password, cust_nric, cust_address, "
				+ "cust_phoneno,  cust_email) values (?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			// set parameter in the query
			if (!cust.getCust_firstname().equals(""))
				ps.setString(1, cust.getCust_firstname());
			else
				ps.setNull(1, java.sql.Types.VARCHAR);
			
			
			if (!cust.getCust_lastname().equals(""))
				ps.setString(2, cust.getCust_lastname());
			else
				ps.setNull(2, java.sql.Types.VARCHAR);
			
			
			if (!cust.getCust_password().equals(""))
				ps.setString(3, cust.getCust_password());
			else
				ps.setNull(3, java.sql.Types.VARCHAR);
			
			
			if (!cust.getCust_nric().equals(""))
				ps.setString(4, cust.getCust_nric());
			else
				ps.setNull(4, java.sql.Types.VARCHAR);
			
			
			if (!cust.getCust_address().equals(""))
				ps.setString(5, cust.getCust_address());
			else
				ps.setNull(5, java.sql.Types.VARCHAR);
			
			
			if (!cust.getCust_phoneno().equals(""))
				ps.setString(6, cust.getCust_phoneno());
			else
				ps.setNull(6, java.sql.Types.VARCHAR);
			
			
			if (!cust.getCust_email().equals(""))
				ps.setString(7, cust.getCust_email());
			else
				ps.setNull(7, java.sql.Types.VARCHAR);
			
			// execute the query
			ps.executeUpdate();
			
			// get generated customer id
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			custId = rs.getInt(1);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}
		
		// return customer id of this new customer to caller. -1 if error
		return custId;
		
	} // end storeCustomer
	
	//-------------------------------------------------------------------------
	
	public static Customer viewCustomer(int id) {
		// the SQL query
		String sql = "SELECT * FROM customer where cust_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		Customer cust = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			// execute query
			resultSet = ps.executeQuery();
			
			if (resultSet.next()) {
				// get values for this record
				int cust_id = resultSet.getInt("cust_id");
				String cust_firstname = resultSet.getString("cust_firstname");
				String cust_lastname = resultSet.getString("cust_lastname");
				String cust_password = resultSet.getString("cust_password");
				String cust_nric = resultSet.getString("cust_nric");
				String cust_address = resultSet.getString("cust_address");
				String cust_phoneno = resultSet.getString("cust_phoneno");
				String cust_email = resultSet.getString("cust_email");
				
				// create Customer bean object based on this record
				cust =  new Customer (cust_id, cust_firstname, cust_lastname, 
						cust_password, cust_nric, cust_address, 
						cust_phoneno,  cust_email);
			}
			
		} // end try
			
		catch (Exception e) {
			e.printStackTrace();
		}
		
		// return the ArrayList with list of Customer object
		return cust;
		
	} // end viewCustomer
	
	// -------------------------------------------------------------------------
	
	public static boolean deleteCustomer(int id ) throws SQLException {
		// delete status
		boolean success = false;
		
		// the SQL query
		String sql = "DELETE FROM customer where cust_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, id);
			// execute the query
			ps.executeUpdate();

			success = true; // successful delete the record

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		// return the status
		return success;
		
	} // end deleteCustomer
	
	// -------------------------------------------------------------------------
	
	public static boolean updateCustomer(Customer cust) throws SQLException {
		
		int numRec = 0; // records updated

		// the SQL query
		String sql = "update Customer set cust_firstname=?, cust_lastname=?"
				+ ", cust_password=?, cust_nric=?, cust_address=?, "
				+ "cust_phoneno=?, cust_email=? where cust_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			// set parameter in the query
			if (!cust.getCust_firstname().equals(""))
				ps.setString(1, cust.getCust_firstname());
			else
				ps.setNull(1, java.sql.Types.VARCHAR);

			if (!cust.getCust_lastname().equals(""))
				ps.setString(2, cust.getCust_lastname());
			else
				ps.setNull(2, java.sql.Types.VARCHAR);

			if (!cust.getCust_password().equals(""))
				ps.setString(3, cust.getCust_password());
			else
				ps.setNull(3, java.sql.Types.VARCHAR);

			if (!cust.getCust_nric().equals(""))
				ps.setString(4, cust.getCust_nric());
			else
				ps.setNull(4, java.sql.Types.VARCHAR);

			if (!cust.getCust_address().equals(""))
				ps.setString(5, cust.getCust_address());
			else
				ps.setNull(5, java.sql.Types.VARCHAR);

			if (!cust.getCust_phoneno().equals(""))
				ps.setString(6, cust.getCust_phoneno());
			else
				ps.setNull(6, java.sql.Types.VARCHAR);

			if (!cust.getCust_email().equals(""))
				ps.setString(7, cust.getCust_email());
			else
				ps.setNull(7, java.sql.Types.VARCHAR);


			ps.setInt(8, cust.getCust_id());

			// execute the query
			numRec = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();

			if (ps != null)
				ps.close();

			if (resultSet != null)
				resultSet.close();
		}

		if (numRec > 0)
			return true;
		else
			return false;

	}
		
} // end class CustomerDAO

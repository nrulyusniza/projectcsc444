package com.csc444.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.csc444.model.Tpackage;
import com.csc444.util.DBUtil;

public class TpackageDAO {
	
	public static List<Tpackage> listAllTpackage() throws SQLException {
	
		// initialize an empty ArrayList
		List<Tpackage> listTpackage = new ArrayList<>();
		
		// the SQL query
		String sql = "SELECT * FROM tpackage";
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			statement = connection.createStatement();
			// execute query
			resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				// get values for this record
				int tpackage_id = resultSet.getInt("tpackage_id");
				String tpackage_name = resultSet.getString("tpackage_name");
				int tpackage_qty = resultSet.getInt("tpackage_qty");
				double tpackage_price = resultSet.getDouble("tpackage_price");
				double tpackage_disc = resultSet.getDouble("tpackage_disc");
				
				// create tpackage bean object based on this record
				Tpackage tpack =  new Tpackage (tpackage_id, tpackage_name, tpackage_qty, 
						tpackage_price, tpackage_disc);
				
				// add this tpackage bean object to the ArrayList
				listTpackage.add(tpack);
			}
			
		} // end try
			
		catch (Exception e) {
			e.printStackTrace();
		}
		
		finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (statement != null)
				statement.close();
	
			if (resultSet != null)
				resultSet.close();
		}
		
		// return the ArrayList with list of Tpackage object
		return listTpackage;	
		
	} // end List<Tpackage>
	
	// -------------------------------------------------------------------------
	
	public static Tpackage getTpackage(int id) throws SQLException {
		
		// Tpackage bean to be returned, default is null
		Tpackage tpack = null;
		
		// the SQL query
		String sql = "SELECT * FROM tpackage where tpackage_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, id);
			// execute the query
			resultSet = ps.executeQuery();
			
			// check if any record returned
			if (resultSet.next()) {
				// if there is a record, get the data
				int tpackage_id = resultSet.getInt("tpackage_id");
				String tpackage_name = resultSet.getString("tpackage_name");
				;
				int tpackage_qty = resultSet.getInt("tpackage_qty");
				;
				double tpackage_price = resultSet.getDouble("tpackage_price");
				;
				double tpackage_disc = resultSet.getDouble("tpackage_disc");
				
				// create Tpackage bean object based on this record
				tpack =  new Tpackage (tpackage_id, tpackage_name, tpackage_qty, 
						tpackage_price, tpackage_disc);
			}
			
		} // end try
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (ps != null)
				ps.close();
	
			if (resultSet != null)
				resultSet.close();
		}
		
		// return the Tpackage bean
		return tpack;
		
	} // end getTpackage
	
	// -------------------------------------------------------------------------
	
	public static Tpackage viewTpackage(int id) {
		// the SQL query
		String sql = "SELECT * FROM tpackage where tpackage_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		Tpackage tpack = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			// execute query
			resultSet = ps.executeQuery();
			
			if (resultSet.next()) {
				// if there is a record, get the data
				int tpackage_id = resultSet.getInt("tpackage_id");
				String tpackage_name = resultSet.getString("tpackage_name");
				int tpackage_qty = resultSet.getInt("tpackage_qty");
				double tpackage_price = resultSet.getDouble("tpackage_price");
				double tpackage_disc = resultSet.getDouble("tpackage_disc");
				
				// create Tpackage bean object based on this record
				tpack =  new Tpackage (tpackage_id, tpackage_name, tpackage_qty, 
						tpackage_price, tpackage_disc);
			}
			
		} // end try
			
		catch (Exception e) {
			e.printStackTrace();
		}
		
		// return the ArrayList with list of Tpackage object
		return tpack;
		
	} // end viewTpackage
	
	// -------------------------------------------------------------------------
	
	public static int storeTpackage(Tpackage tpack) throws SQLException {
		// to store tpackage id of this new tpackage
		int tpackId = -1;
		
		// the SQL query
		String sql = "insert into Tpackage (tpackage_name, tpackage_qty, "
				+ "tpackage_price, tpackage_disc) values (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			// set parameter in the query
			if (!tpack.getTpackage_name().equals(""))
				ps.setString(1, tpack.getTpackage_name());
			else
				ps.setNull(1, java.sql.Types.VARCHAR);
			
			
			if (tpack.getTpackage_qty() != -1)
				ps.setInt(2, tpack.getTpackage_qty());
			else
				ps.setNull(2, java.sql.Types.INTEGER);
			
			
			ps.setDouble(3, tpack.getTpackage_price());
			
			
			ps.setDouble(4, tpack.getTpackage_disc());
			
			
			// execute the query
			ps.executeUpdate();
			
			// get generated tpackage id
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			tpackId = rs.getInt(1);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (ps != null)
				ps.close();
	
			if (resultSet != null)
				resultSet.close();
		}
		
		// return tpackage id of this new tpackage to caller. -1 if error
		return tpackId;
		
	} // end storeAddItem
	
	// -------------------------------------------------------------------------
	
	public static boolean deleteTpackage(int id ) throws SQLException {
		// delete status
		boolean success = false;
		
		// the SQL query
		String sql = "DELETE FROM tpackage where tpackage_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, id);
			// execute the query
			ps.executeUpdate();
	
			success = true; // successful delete the record
	
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (ps != null)
				ps.close();
	
			if (resultSet != null)
				resultSet.close();
		}
	
		// return the status
		return success;
		
	} // end deleteTpackage
	
	// -------------------------------------------------------------------------
	
	public static boolean updateTpackage(Tpackage tpack) throws SQLException {
		
		int numRec = 0; // records updated
		
		// the SQL query
		String sql = "update into Tpackage set tpackage_name=?, tpackage_qty=?"
				+ ", tpackage_price=?, tpackage_disc=? where tpackage_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			// set parameter in the query
			if (!tpack.getTpackage_name().equals(""))
				ps.setString(1, tpack.getTpackage_name());
			else
				ps.setNull(1, java.sql.Types.VARCHAR);
			
			
			if (tpack.getTpackage_qty() != -1)
				ps.setInt(2, tpack.getTpackage_qty());
			else
				ps.setNull(2, java.sql.Types.INTEGER);
			
			
			ps.setDouble(3, tpack.getTpackage_price());
			
			
			ps.setDouble(4, tpack.getTpackage_disc());
			
			
			ps.setInt(5, tpack.getTpackage_id());
			
			// execute the query
			numRec = ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (ps != null)
				ps.close();
	
			if (resultSet != null)
				resultSet.close();
		}
	
		if (numRec > 0)
			return true;
		else
			return false;
		
	} // end updateTpackage

} // end class TpackageDAO

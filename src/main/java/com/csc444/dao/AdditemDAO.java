package com.csc444.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.csc444.model.Additem;
import com.csc444.util.DBUtil;

public class AdditemDAO {
	
	public static List<Additem> listAllAdditem() throws SQLException {
	
		// initialize an empty ArrayList
		List<Additem> listAdditem = new ArrayList<>();
		
		// the SQL query
		String sql = "SELECT * FROM additem";
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			statement = connection.createStatement();
			// execute query
			resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				// get values for this record
				int additem_id = resultSet.getInt("additem_id");
				String additem_name = resultSet.getString("additem_name");
				int additem_qty = resultSet.getInt("additem_qty");
				double additem_price = resultSet.getDouble("additem_price");
				double additem_disc = resultSet.getDouble("additem_disc");
				
				// create AddItem bean object based on this record
				Additem addit =  new Additem (additem_id, additem_name, additem_qty, 
						additem_price, additem_disc);
				
				// add this AddItem bean object to the ArrayList
				listAdditem.add(addit);
			}
			
		} // end try
			
		catch (Exception e) {
			e.printStackTrace();
		}
		
		finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (statement != null)
				statement.close();
	
			if (resultSet != null)
				resultSet.close();
		}
		
		// return the ArrayList with list of AddItem object
		return listAdditem;	
		
	} // end List<Additem>
	
	// -------------------------------------------------------------------------
	
	public static Additem getAdditem(int id) throws SQLException {
		
		// Customer bean to be returned, default is null
		Additem addit = null;
		
		// the SQL query
		String sql = "SELECT * FROM additem where additem_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, id);
			// execute the query
			resultSet = ps.executeQuery();
			
			// check if any record returned
			if (resultSet.next()) {
				// if there is a record, get the data
				int additem_id = resultSet.getInt("additem_id");
				String additem_name = resultSet.getString("additem_name");
				int additem_qty = resultSet.getInt("additem_qty");
				double additem_price = resultSet.getDouble("additem_price");
				double additem_disc = resultSet.getDouble("additem_disc");
				
				// create Additem bean object based on this record
				addit =  new Additem (additem_id, additem_name, additem_qty, 
						additem_price, additem_disc);
			}
			
		} // end try
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (ps != null)
				ps.close();
	
			if (resultSet != null)
				resultSet.close();
		}
		
		// return the Additem bean
		return addit;
		
	} // end getAdditem
	
	// -------------------------------------------------------------------------
	
	public static Additem viewAdditem(int id) {
		// the SQL query
		String sql = "SELECT * FROM additem where additem_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		Additem addit = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			// execute query
			resultSet = ps.executeQuery();
			
			if (resultSet.next()) {
				// get values for this record
				int additem_id = resultSet.getInt("additem_id");
				String additem_name = resultSet.getString("additem_name");
				int additem_qty = resultSet.getInt("additem_qty");
				double additem_price = resultSet.getDouble("additem_price");
				double additem_disc = resultSet.getDouble("additem_disc");
				
				// create Additem bean object based on this record
				addit =  new Additem (additem_id, additem_name, additem_qty, 
						additem_price, additem_disc);
			}
			
		} // end try
			
		catch (Exception e) {
			e.printStackTrace();
		}
		
		// return the ArrayList with list of Additem object
		return addit;
		
	} // end viewAdditem
	
	// -------------------------------------------------------------------------
	
	public static int storeAdditem(Additem addit) throws SQLException {
		// to store additem id of this new additem
		int additId = -1;
		
		// the SQL query
		String sql = "insert into additem (additem_name, additem_qty, "
				+ "additem_price, additem_disc) values (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			// set parameter in the query
			if (!addit.getAdditem_name().equals(""))
				ps.setString(1, addit.getAdditem_name());
			else
				ps.setNull(1, java.sql.Types.VARCHAR);
			
			
			ps.setDouble(2, addit.getAdditem_qty());
			
			
			if (addit.getAdditem_price() != -1)
				ps.setDouble(3, addit.getAdditem_price());
			else
				ps.setNull(3, java.sql.Types.DOUBLE);
			
			
			if (addit.getAdditem_disc() != -1)
				ps.setDouble(4, addit.getAdditem_disc());
			else
				ps.setNull(4, java.sql.Types.DOUBLE);
			
			// execute the query
			ps.executeUpdate();
			
			// get generated additem id
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			additId = rs.getInt(1);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (ps != null)
				ps.close();
	
			if (resultSet != null)
				resultSet.close();
		}
		
		// return additem id of this new additem to caller. -1 if error
		return additId;
		
	} // end storeAddItem
	
	// -------------------------------------------------------------------------
	
	public static boolean deleteAdditem(int id ) throws SQLException {
		// delete status
		boolean success = false;
		
		// the SQL query
		String sql = "DELETE FROM additem where additem_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, id);
			// execute the query
			ps.executeUpdate();
	
			success = true; // successful delete the record
	
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (ps != null)
				ps.close();
	
			if (resultSet != null)
				resultSet.close();
		}
	
		// return the status
		return success;
		
	} // end deleteAdditem
	
	// -------------------------------------------------------------------------
	
	public static boolean updateAdditem(Additem addit) throws SQLException {
		
		int numRec = 0; // records updated
		
		// the SQL query
		String sql = "update into additem set additem_name=?, additem_qty=?, "
				+ "additem_price=?, additem_disc=?) where additem_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			// set parameter in the query
			if (!addit.getAdditem_name().equals(""))
				ps.setString(1, addit.getAdditem_name());
			else
				ps.setNull(1, java.sql.Types.VARCHAR);
			
			
			if (addit.getAdditem_qty() != -1)
				ps.setInt(2, addit.getAdditem_qty());
			else
				ps.setNull(2, java.sql.Types.INTEGER);
			
			
			ps.setDouble(2, addit.getAdditem_qty());
			
			
			if (addit.getAdditem_disc() != -1)
				ps.setDouble(4, addit.getAdditem_disc());
			else
				ps.setNull(4, java.sql.Types.DOUBLE);
			
			ps.setInt(5, addit.getAdditem_id());
			
			// execute the query
			numRec = ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (ps != null)
				ps.close();
	
			if (resultSet != null)
				resultSet.close();
		}
	
		if (numRec > 0)
			return true;
		else
			return false;
		
	} // end updateAdditem

} // end class AdditemDAO

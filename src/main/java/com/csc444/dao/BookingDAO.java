package com.csc444.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.csc444.model.Booking;
import com.csc444.util.DBUtil;

public class BookingDAO {
	
	public static List<Booking> listAllBooking() throws SQLException {
		
		// initialize an empty ArrayList
		List<Booking> listBooking = new ArrayList<>();
		
		// the SQL query
		String sql = "SELECT * FROM booking";
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			statement = connection.createStatement();
			// execute query
			resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				// get values for this record
				int booking_id = resultSet.getInt("booking_id");
				int cust_id = resultSet.getInt("cust_id");
				int tpackage_id = resultSet.getInt("tpackage_id");
				Date booking_date = resultSet.getDate("booking_date");
				int admin_id = resultSet.getInt("admin_id");
				
				// create booking bean object based on this record
				Booking book =  new Booking (booking_id, cust_id, tpackage_id, 
						booking_date, admin_id);
				
				// add this booking bean object to the ArrayList
				listBooking.add(book);
			}
			
		} // end try
			
		catch (Exception e) {
			e.printStackTrace();
		}
		
		finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (statement != null)
				statement.close();
	
			if (resultSet != null)
				resultSet.close();
		}
		
		// return the ArrayList with list of Booking object
		return listBooking;	
		
	} // end List<Booking>
	
	// -------------------------------------------------------------------------
	
	public static Booking getBooking(int id) throws SQLException {
		
		// Booking bean to be returned, default is null
		Booking book = null;
		
		// the SQL query
		String sql = "SELECT * FROM booking where booking_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, id);
			// execute the query
			resultSet = ps.executeQuery();
			
			// check if any record returned
			if (resultSet.next()) {
				// if there is a record, get the data
				int booking_id = resultSet.getInt("booking_id");
				int cust_id = resultSet.getInt("cust_id");
				int tpackage_id = resultSet.getInt("tpackage_id");
				Date booking_date = resultSet.getDate("booking_date");
				int admin_id = resultSet.getInt("admin_id");
				
				// create Booking bean object based on this record
				book =   new Booking (booking_id, cust_id, tpackage_id, 
						booking_date, admin_id);
			}
			
		} // end try
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (ps != null)
				ps.close();
	
			if (resultSet != null)
				resultSet.close();
		}
		
		// return the Booking bean
		return book;
		
	} // end getBooking
	
	// -------------------------------------------------------------------------
	
	public static Booking viewBooking(int id) {
		// the SQL query
		String sql = "SELECT * FROM booking where booking_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		Booking book = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			// execute query
			resultSet = ps.executeQuery();
			
			if (resultSet.next()) {
				// if there is a record, get the data
				int booking_id = resultSet.getInt("booking_id");
				int cust_id = resultSet.getInt("cust_id");
				int tpackage_id = resultSet.getInt("tpackage_id");
				Date booking_date = resultSet.getDate("booking_date");
				int admin_id = resultSet.getInt("admin_id");
				
				// create Booking bean object based on this record
				book =  new Booking (booking_id, cust_id, tpackage_id, 
						booking_date, admin_id);
			}
			
		} // end try
			
		catch (Exception e) {
			e.printStackTrace();
		}
		
		// return the ArrayList with list of Booking object
		return book;
		
	} // end viewBooking
	
	// -------------------------------------------------------------------------
	
	public static int storeBooking(Booking book) throws SQLException {
		// to store booking id of this new booking
		int bookId = -1;
		
		// the SQL query
		String sql = "insert into booking (cust_id, tpackage_id, "
				+ "booking_date, admin_id) values (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			// set parameter in the query
			if (book.getCust_id() != -1)
				ps.setInt(1, book.getCust_id());
			else
				ps.setNull(1, java.sql.Types.INTEGER);
			
			
			if (book.getTpackage_id() != -1)
				ps.setInt(2, book.getTpackage_id());
			else
				ps.setNull(2, java.sql.Types.INTEGER);
			
			
			if (book.getBooking_date() != null) {
				// need to convert from java.util.Date to java.sql.Date
				java.sql.Date sqlDate = new java.sql.Date(book.getBooking_date().getTime());
				ps.setDate(3, sqlDate);
			} else {
				ps.setNull(3, java.sql.Types.DATE);
			}
			
			
			if (book.getAdmin_id() != -1)
				ps.setInt(4, book.getAdmin_id());
			else
				ps.setNull(4, java.sql.Types.INTEGER);
			
			// execute the query
			ps.executeUpdate();
			
			// get generated booking id
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			bookId = rs.getInt(1);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (ps != null)
				ps.close();
	
			if (resultSet != null)
				resultSet.close();
		}
		
		// return booking id of this new booking to caller. -1 if error
		return bookId;
		
	} // end storeAddItem
	
	// -------------------------------------------------------------------------
	
	public static boolean deleteBooking(int id ) throws SQLException {
		// delete status
		boolean success = false;
		
		// the SQL query
		String sql = "DELETE FROM booking where booking_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			// prepare statement
			ps = connection.prepareStatement(sql);
			// set parameter in the query
			ps.setInt(1, id);
			// execute the query
			ps.executeUpdate();
	
			success = true; // successful delete the record
	
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (ps != null)
				ps.close();
	
			if (resultSet != null)
				resultSet.close();
		}
	
		// return the status
		return success;
		
	} // end deleteBooking
	
	// -------------------------------------------------------------------------
	
	public static boolean updateBooking(Booking book) throws SQLException {
		
		int numRec = 0; // records updated
		
		// the SQL query
		String sql = "update into booking set cust_id=?, tpackage_id=?, "
				+ "booking_date=?, admin_id=?) where booking_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		
		try {
			// get connection from connection pool
			connection = DBUtil.getDataSource().getConnection();
			
			// prepare statement, return the auto increment id
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			// set parameter in the query
			if (book.getCust_id() != -1)
				ps.setInt(1, book.getCust_id());
			else
				ps.setNull(1, java.sql.Types.INTEGER);
			
			
			if (book.getTpackage_id() != -1)
				ps.setInt(2, book.getTpackage_id());
			else
				ps.setNull(2, java.sql.Types.INTEGER);
			
			
			if (book.getBooking_date() != null) {
				// need to convert from java.util.Date to java.sql.Date
				java.sql.Date sqlDate = new java.sql.Date(book.getBooking_date().getTime());
				ps.setDate(3, sqlDate);
			} else {
				ps.setNull(3, java.sql.Types.DATE);
			}
			
			
			if (book.getAdmin_id() != -1)
				ps.setInt(4, book.getAdmin_id());
			else
				ps.setNull(4, java.sql.Types.INTEGER);
			
			ps.setInt(5, book.getBooking_id());
			
			// execute the query
			numRec = ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// close connection after finish
			if (connection != null)
				connection.close();
	
			if (ps != null)
				ps.close();
	
			if (resultSet != null)
				resultSet.close();
		}
	
		if (numRec > 0)
			return true;
		else
			return false;
		
	} // end updateBooking

} // end class BookingDAO

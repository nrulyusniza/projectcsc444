package com.csc444.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.csc444.dao.TpackageDAO;
import com.csc444.model.Tpackage;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.annotation.WebServlet;

/**
 * Servlet implementation class TpackageServlet
 */
@WebServlet("/TpackageServlet")
public class TpackageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public TpackageServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get action parameter
		String action = request.getParameter("action");
		
		if (action.equals("list")) {
			listAllTpackage(request, response);
		}
		else if (action.equals("view")) {
			viewTpackage(request, response);
		}
		else if (action.equals("create")) {
			createTpackage(request, response);
		}
		else if (action.equals("delete")) {
			deleteTpackage(request, response);
		}
		else if (action.equals("edit")) {
			editTpackage(request, response);
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get action parameter
		String action = request.getParameter("action");
		
		if (action.equals("store")) {
			storeTpackage(request, response);
		}
		if (action.equals("update")) {
			updateTpackage(request, response);
		}
	}

	
	protected void listAllTpackage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			List<Tpackage> tpackage = TpackageDAO.listAllTpackage();
			
			// set attributes
			request.setAttribute("tpackage", tpackage);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("tpackage/tpackage_index.jsp");
			requestDispatcher.forward(request,response);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	protected void viewTpackage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		
		// get Tpackage record based on the id
		Tpackage tpack = TpackageDAO.viewTpackage(id);
		
		// set attributes
		request.setAttribute("tpack", tpack);
		
		// forward to JSP
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("tpackage/tpackage_view.jsp");
		requestDispatcher.forward(request,response);
	}
	
	
	protected void createTpackage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("tpackage/tpackage_create.jsp");
			requestDispatcher.forward(request,response);
	}
	
	
	protected void storeTpackage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {		
			// get the form fields
			String tpackagename = request.getParameter("tpackage_name");
			String tpackageqtyStr = request.getParameter("tpackage_qty");
			String tpackagepriceStr = request.getParameter("tpackage_price");
			String tpackagediscStr = request.getParameter("tpackage_disc");
			
			// set to object
			Tpackage tpack = new Tpackage();
			tpack.setTpackage_name(tpackagename);
			tpack.setTpackage_qty(0);
			
			double tpackagePrice = Double.parseDouble(tpackagepriceStr);
			tpack.setTpackage_price(tpackagePrice);
			
			double tpackageDisc = Double.parseDouble(tpackagediscStr);
			tpack.setTpackage_disc(tpackageDisc);
			
			//store to database
			int tpackId = TpackageDAO.storeTpackage(tpack);
			
			// redirect user to tpackage detail page
			response.sendRedirect("TpackageServlet?action=view&id=" + tpackId);
			
		} /*catch (ParseException e) {
			e.printStackTrace();
		} */catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	protected void deleteTpackage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get tpackage id
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// delete tpackage record based on the id
			TpackageDAO.deleteTpackage(id);
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		// redirect user to tpackage list page
		response.sendRedirect("TpackageServlet?action=list");
		
	}
	
	
	protected void editTpackage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get tpackage id
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// get tpackage record
			Tpackage tpack = TpackageDAO.getTpackage(id);
			
			// set attribute
			request.setAttribute("tpack", tpack);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("tpackage/tpackage_update.jsp");
			requestDispatcher.forward(request, response);
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
	}
	
	
	protected void updateTpackage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {		
			// get the form fields
			String tpackagename = request.getParameter("tpackage_name");
			String tpackageqtyStr = request.getParameter("tpackage_qty");
			String tpackagepriceStr = request.getParameter("tpackage_price");
			String tpackagediscStr = request.getParameter("tpackage_disc");
			int tpackageqty = Integer.parseInt(request.getParameter("tpackage_qty"));
			String tpackageprice = request.getParameter("tpackage_price");
			String tpackagedisc = request.getParameter("tpackage_disc");

			int tpackId = Integer.parseInt(request.getParameter("tpackage_id"));
			int tpackageid = Integer.parseInt(request.getParameter("tpackage_id"));

			// set to object
			Tpackage tpack = new Tpackage();
			tpack.setTpackage_id(tpackId);
			tpack.setTpackage_name(tpackagename);
			tpack.setTpackage_qty(0);
			tpack.setTpackage_id(tpackageid);
			tpack.setTpackage_name(tpackagename);
			tpack.setTpackage_qty(tpackageqty);

			double tpackagePrice = Double.parseDouble(tpackagepriceStr);
			tpack.setTpackage_price(tpackagePrice);
			
			double tpackageDisc = Double.parseDouble(tpackagediscStr);
			tpack.setTpackage_disc(tpackageDisc);
			
			//store to database
			boolean updateStatus = TpackageDAO.updateTpackage(tpack);
			
			// redirect user to tpackage detail page
			response.sendRedirect("TpackageServlet?action=view&id=" + tpackId);
			response.sendRedirect("TpackageServlet?action=view&id=" + tpackageid);	
		} /*catch (ParseException e) {
			e.printStackTrace();
		}*/ catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

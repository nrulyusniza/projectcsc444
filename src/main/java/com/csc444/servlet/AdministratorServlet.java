package com.csc444.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import com.csc444.dao.AdministratorDAO;
import com.csc444.model.Administrator;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.annotation.WebServlet;

/**
 * Servlet implementation class AdministratorServlet
 */
@WebServlet("/AdministratorServlet")
public class AdministratorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public AdministratorServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get action parameter
		String action = request.getParameter("action");
		
		if (action.equals("list")) {
			listAllAdministrator(request, response);
		}
		else if (action.equals("view")) {
			viewAdministrator(request, response);
		}
		else if (action.equals("create")) {
			createAdministrator(request, response);
		}
		else if (action.equals("delete")) {
			deleteAdministrator(request, response);
		}
		else if (action.equals("edit")) {
			editAdministrator(request, response);
		}
		else if (action.equals("logout")) {
			logoutAdministrator(request, response);
		}
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get action parameter
		String action = request.getParameter("action");
		
		if (action.equals("store")) {
			storeAdministrator(request, response);
		}
		if (action.equals("update")) {
			updateAdministrator(request, response);
		}
		else if (action.equals("login")) {
			loginAdministrator(request, response);
		}
	}

/**
* List all administrator record
*/	
	protected void listAllAdministrator(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			List<Administrator> administrator = AdministratorDAO.listAllAdministrator();
			
			// set attributes
			request.setAttribute("administrator", administrator);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("administrator/administrator_list.jsp");
			requestDispatcher.forward(request,response);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	protected void viewAdministrator(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		
		// get Administrator record based on the id
		Administrator admin = AdministratorDAO.viewAdministrator(id);
		
		// set attributes
		request.setAttribute("admin", admin);
		
		// forward to JSP
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("administrator/administrator_view.jsp");
		requestDispatcher.forward(request,response);
	}
	
	protected void createAdministrator(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("administrator/administrator_create.jsp");
			requestDispatcher.forward(request,response);
		}
	
	protected void storeAdministrator(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {		
			// get the form fields
			String adminfirstname = request.getParameter("admin_firstname");
			String adminlastname = request.getParameter("admin_lastname");
			String adminpassword = request.getParameter("admin_password");
			String adminnric = request.getParameter("admin_nric");
			String adminphoneno = request.getParameter("admin_phoneno");
			String adminemail = request.getParameter("admin_email");
			
			// set to object
			Administrator admin = new Administrator();
			admin.setAdmin_firstname(adminfirstname);
			admin.setAdmin_lastname(adminlastname);
			admin.setAdmin_password(adminpassword);
			admin.setAdmin_nric(adminnric);
			admin.setAdmin_phoneno(adminphoneno);
			admin.setAdmin_email(adminemail);
			
			//store to database
			int adminId = AdministratorDAO.storeAdministrator(admin);
			
			// redirect user to customer detail page
			response.sendRedirect("AdministratorServlet?action=view&id=" + adminId);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	protected void deleteAdministrator(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get customer id
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// delete Customer record based on the id
			AdministratorDAO.deleteAdministrator(id);
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		// redirect user to administrator list page
		response.sendRedirect("AdministratorServlet?action=list");
		
	}
	
	
	protected void editAdministrator(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get customer id
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// get customer record
			Administrator admin = AdministratorDAO.getAdministrator(id);
			
			// set to request attribute
			request.setAttribute("admin", admin);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("administrator/administrator_update.jsp");
			requestDispatcher.forward(request, response);
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
	}
	
	
	protected void updateAdministrator(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {		
			// get the form fields
			String adminfirstname = request.getParameter("admin_firstname");
			String adminlastname = request.getParameter("admin_lastname");
			String adminpassword = request.getParameter("admin_password");
			String adminnric = request.getParameter("admin_nric");
			String adminphoneno = request.getParameter("admin_phoneno");
			String adminemail = request.getParameter("admin_email");
			
			int adminId = Integer.parseInt(request.getParameter("admin_id"));
			
			// set to object
			Administrator admin = new Administrator();
			admin.setAdmin_id(adminId);
			admin.setAdmin_firstname(adminfirstname);
			admin.setAdmin_lastname(adminlastname);
			admin.setAdmin_password(adminpassword);
			admin.setAdmin_nric(adminnric);
			admin.setAdmin_phoneno(adminphoneno);
			admin.setAdmin_email(adminemail);
			
			// store to DB
			boolean updateStatus = AdministratorDAO.updateAdministrator(admin);
						
			
			// redirect user to admin detail page
			response.sendRedirect("AdministratorServlet?action=view&id=" + adminId);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * Login administrator
	 */
	
    protected void loginAdministrator(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	String admin_email = request.getParameter("admin_email");
    	String admin_password = request.getParameter("admin_password");
    	
		try {
			// login based on email and password
			Administrator admin = AdministratorDAO.login(admin_email, admin_password);
			
			if (admin != null) {
			
				// set attribute
				HttpSession session = request.getSession();
				session.setAttribute("admin", admin);
				
				// forward to JSP
				response.sendRedirect("administrator/administrator_index.jsp");
			}
			else {
				response.sendRedirect("administrator/administrator_login.jsp?message=Invalid username/password");

			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
	}
    
    protected void logoutAdministrator(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	request.getSession().invalidate();	// destroy session
    	
    	// forward to home
		response.sendRedirect("index.html");

		
	}
	
	
		
}

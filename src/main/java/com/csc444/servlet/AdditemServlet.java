package com.csc444.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import com.csc444.dao.AdditemDAO;
import com.csc444.model.Additem;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import jakarta.servlet.RequestDispatcher;

/**
 * Servlet implementation class AdditemServlet
 */
@WebServlet("/AdditemServlet")
public class AdditemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public AdditemServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get action parameter
		String action = request.getParameter("action");
		
		if (action.equals("list")) {
			listAllAdditem(request, response);
		}
		else if (action.equals("view")) {
			viewAdditem(request, response);
		}
		else if (action.equals("create")) {
			//createAdditem(request, response);
		}
		else if (action.equals("delete")) {
			deleteAdditem(request, response);
		}
		else if (action.equals("edit")) {
			editAdditem(request, response);
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get action parameter
		String action = request.getParameter("action");
		
		if (action.equals("store")) {
			storeAdditem(request, response);
		}
		if (action.equals("update")) {
			updateAdditem(request, response);
		}
	}

	
	protected void listAllAdditem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			List<Additem> additem = AdditemDAO.listAllAdditem();
			
			// set attributes
			request.setAttribute("additem", additem);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("additem/additem_index.jsp");
			requestDispatcher.forward(request,response);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	protected void viewAdditem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		
		// get Additem record based on the id
		Additem itemadd = AdditemDAO.viewAdditem(id);
		
		// set attributes
		request.setAttribute("itemadd", itemadd);
		
		// forward to JSP
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("additem/additem_view.jsp");
		requestDispatcher.forward(request,response);
	}
	
	/*
	protected void createAdditem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			
						
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("additem/additem_create.jsp");
			requestDispatcher.forward(request,response);
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
	}*/
	
	
	protected void storeAdditem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {		
			// get the form fields
			String additemName = request.getParameter("additemName");
			String additemQtyStr = request.getParameter("additemQty");
			String additemPriceStr = request.getParameter("additemPrice");
			String additemDiscStr = request.getParameter("additemDisc");
			
			// set to object
			Additem addit = new Additem();
			addit.setAdditem_name(additemName);
			addit.setAdditem_qty(0);
			
			double additemPrice = Double.parseDouble(additemPriceStr);
			addit.setAdditem_price(additemPrice);
			
			double additemDisc = Double.parseDouble(additemDiscStr);
			addit.setAdditem_disc(additemDisc);
			
			//store to database
			int additemId = AdditemDAO.storeAdditem(addit);
			
			// redirect user to additem detail page
			response.sendRedirect("AdditemServlet?action=view&id=" + additemId);
			
		}/* catch (ParseException e) {
			e.printStackTrace();
		}*/ catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	protected void deleteAdditem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get additem id
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// delete additem record based on the id
			AdditemDAO.deleteAdditem(id);
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		// redirect user to additem list page
		response.sendRedirect("AdditemServlet?action=list");
		
	}
	
	
	protected void editAdditem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get additem id
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// get additem record
			Additem addit = AdditemDAO.getAdditem(id);
			
			// set attribute
			request.setAttribute("addit", addit);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("additem/additem_update.jsp");
			requestDispatcher.forward(request, response);
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
	}
	
	
	protected void updateAdditem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {		
			// get the form fields
			String additemName = request.getParameter("additemName");
			String additemQtyStr = request.getParameter("additemQty");
			String additemPriceStr = request.getParameter("additemPrice");
			String additemDiscStr = request.getParameter("additemDisc");
			
			int additem_id = Integer.parseInt(request.getParameter("additem_id"));
			
			// set to object
			Additem addit = new Additem();
			addit.setAdditem_name(additemName);
			addit.setAdditem_qty(0);
			
			double additemPrice = Double.parseDouble(additemPriceStr);
			addit.setAdditem_price(additemPrice);
			
			double additemDisc = Double.parseDouble(additemDiscStr);
			addit.setAdditem_disc(additemDisc);
			
			//store to database
			boolean updateStatus = AdditemDAO.updateAdditem(addit);
			
			// redirect user to customer detail page
			response.sendRedirect("AdditemServlet?action=view&id=" + additem_id);
			
		} /*catch (ParseException e) {
			e.printStackTrace();
		}*/ catch (SQLException e) {
			e.printStackTrace();
		}
	}

}

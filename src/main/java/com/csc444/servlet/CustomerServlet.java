package com.csc444.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import com.csc444.dao.AdministratorDAO;
import com.csc444.dao.CustomerDAO;
import com.csc444.model.Administrator;
import com.csc444.model.Customer;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.annotation.WebServlet;

/**
 * Servlet implementation class CustomerServlet
 */
@WebServlet("/CustomerServlet")
public class CustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public CustomerServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get action parameter
		String action = request.getParameter("action");
		
		if (action.equals("list")) {
			listAllCustomer(request, response);
		}
		else if (action.equals("view")) {
			viewCustomer(request, response);
		}
		else if (action.equals("create")) {
			createCustomer(request, response);
		}
		else if (action.equals("delete")) {
			deleteCustomer(request, response);
		}
		else if (action.equals("edit")) {
			editCustomer(request, response);
		}
		else if (action.equals("logout")) {
			logoutCustomer(request, response);
		}
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get action parameter
		String action = request.getParameter("action");
		
		if (action.equals("store")) {
			storeCustomer(request, response);
		}
		if (action.equals("update")) {
			updateCustomer(request, response);
		}
		else if (action.equals("login")) {
			loginCustomer(request, response);
		}
	}
	
	/**
	 * Login administrator
	 */
	
    protected void loginCustomer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	String cust_email = request.getParameter("cust_email");
    	String cust_password = request.getParameter("cust_password");
    	
		try {
			// login based on email and password
			Customer cust = CustomerDAO.login(cust_email, cust_password);
			
			if (cust != null) {
			
				// set attribute
				HttpSession session = request.getSession();
				session.setAttribute("cust", cust);
				
				// forward to JSP
				response.sendRedirect("booking/booking_index.jsp");
			}
			else {
				response.sendRedirect("customer/customer_login.jsp?message=Invalid username/password");

			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
	}
    
    protected void logoutCustomer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	request.getSession().invalidate();	// destroy session
    	
    	// forward to home
		response.sendRedirect("index.html");

		
	}

/**
* List all customer record
*/	
	protected void listAllCustomer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			List<Customer> customer = CustomerDAO.listAllCustomer();
			
			// set attributes
			request.setAttribute("customer", customer);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("customer/customer_list.jsp");
			requestDispatcher.forward(request,response);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	protected void viewCustomer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		
		// get Customer record based on the id
		Customer cust = CustomerDAO.viewCustomer(id);
		
		// set attributes
		request.setAttribute("cust", cust);
		
		// forward to JSP
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("customer/customer_view.jsp");
		requestDispatcher.forward(request,response);
	}
	
	protected void createCustomer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("customer/customer_create.jsp");
			requestDispatcher.forward(request,response);
		}
	
	protected void storeCustomer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {		
			// get the form fields
			String custfirstname = request.getParameter("cust_firstname");
			String custlastname = request.getParameter("cust_lastname");
			String custpassword = request.getParameter("cust_password");
			String custnric = request.getParameter("cust_nric");
			String custaddress = request.getParameter("cust_address");
			String custphoneno = request.getParameter("cust_phoneno");
			String custemail = request.getParameter("cust_email");
			
			// set to object
			Customer cust = new Customer();
			cust.setCust_firstname(custfirstname);
			cust.setCust_lastname(custlastname);
			cust.setCust_password(custpassword);
			cust.setCust_nric(custnric);
			cust.setCust_address(custaddress);
			cust.setCust_phoneno(custphoneno);
			cust.setCust_email(custemail);
			
			//store to database
			int custId = CustomerDAO.storeCustomer(cust);
			
			// redirect user to customer detail page
			response.sendRedirect("CustomerServlet?action=view&id=" + custId);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	protected void deleteCustomer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get customer id
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// delete Customer record based on the id
			CustomerDAO.deleteCustomer(id);
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		// redirect user to customer list page
		response.sendRedirect("CustomerServlet?action=list");
		
	}
	
	
	protected void editCustomer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get customer id
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// get customer record
			Customer cust = CustomerDAO.getCustomer(id);
			
			// set to request attribute
			request.setAttribute("cust", cust);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("customer/customer_update.jsp");
			requestDispatcher.forward(request, response);
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
	}
	
	
	protected void updateCustomer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {		
			// get the form fields
			String custfirstname = request.getParameter("cust_firstname");
			String custlastname = request.getParameter("cust_lastname");
			String custpassword = request.getParameter("cust_password");
			String custnric = request.getParameter("cust_nric");
			String custaddress = request.getParameter("cust_address");
			String custphoneno = request.getParameter("cust_phoneno");
			String custemail = request.getParameter("cust_email");
			
			int custId = Integer.parseInt(request.getParameter("cust_id"));
			
			// set to object
			Customer cust = new Customer();
			cust.setCust_id(custId);
			cust.setCust_firstname(custfirstname);
			cust.setCust_lastname(custlastname);
			cust.setCust_password(custpassword);
			cust.setCust_nric(custnric);
			cust.setCust_address(custaddress);
			cust.setCust_phoneno(custphoneno);
			cust.setCust_email(custemail);
			
			// store to DB
			boolean updateStatus = CustomerDAO.updateCustomer(cust);
						
			
			// redirect user to customer detail page
			response.sendRedirect("CustomerServlet?action=view&id=" + custId);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		
}

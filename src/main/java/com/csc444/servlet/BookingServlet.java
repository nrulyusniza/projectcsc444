package com.csc444.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.csc444.dao.AdministratorDAO;
import com.csc444.dao.BookingDAO;
import com.csc444.dao.CustomerDAO;
import com.csc444.dao.TpackageDAO;
import com.csc444.model.Administrator;
import com.csc444.model.Booking;
import com.csc444.model.Customer;
import com.csc444.model.Tpackage;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.annotation.WebServlet;

/**
 * Servlet implementation class BookingServlet
 */
@WebServlet("/BookingServlet")
public class BookingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public BookingServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get action parameter
		String action = request.getParameter("action");
		
		if (action.equals("list")) {
			listAllBooking(request, response);
		}
		else if (action.equals("view")) {
			viewBooking(request, response);
		}
		else if (action.equals("create")) {
			//createBooking(request, response);
		}
		else if (action.equals("delete")) {
			deleteBooking(request, response);
		}
		else if (action.equals("edit")) {
			editBooking(request, response);
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get action parameter
		String action = request.getParameter("action");
		
		if (action.equals("store")) {
			storeBooking(request, response);
		}
		if (action.equals("update")) {
			updateBooking(request, response);
		}
	}

	
	protected void listAllBooking(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			List<Booking> booking = BookingDAO.listAllBooking();
			
			// set attributes
			request.setAttribute("booking", booking);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("booking/booking_index.jsp");
			requestDispatcher.forward(request,response);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	protected void viewBooking(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		
		// get Booking record based on the id
		Booking book = BookingDAO.viewBooking(id);
		
		// set attributes
		request.setAttribute("book", book);
		
		// forward to JSP
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("booking/booking_view.jsp");
		requestDispatcher.forward(request,response);
	}
	
	protected void createBooking(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			// get list of FK
			List<Customer> customer = CustomerDAO.listAllCustomer();
			List<Tpackage> tpackage = TpackageDAO.listAllTpackage();
			List<Administrator> administrator = AdministratorDAO.listAllAdministrator();
			
			// set attributes
			request.setAttribute("customer", customer);
			request.setAttribute("tpackage", tpackage);
			request.setAttribute("administrator", administrator);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("booking/booking_update.jsp");
			requestDispatcher.forward(request, response);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	
	protected void storeBooking(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {		
			// get the form fields
			String custIdStr = request.getParameter("cust_id");
			String tpackageIdStr = request.getParameter("tpackage_id");
			String bookingDateStr = request.getParameter("booking_date");
			String adminIdStr = request.getParameter("admin_id");
			
			// set to object
			Booking book = new Booking();
			int custId = -1;
			if (!custIdStr.equals(""))
				custId = Integer.parseInt(custIdStr);
			book.setCust_id(custId);
			
			
			int tpaId = -1;
			if (!tpackageIdStr.equals(""))
				tpaId = Integer.parseInt(tpackageIdStr);
			book.setTpackage_id(tpaId);
			
			Date booking_date=new SimpleDateFormat("yyyy-MM-dd").parse(bookingDateStr);
			book.setBooking_date(booking_date);
			
			
			int admId = -1;
			if (!adminIdStr.equals(""))
				admId = Integer.parseInt(adminIdStr);
			book.setAdmin_id(admId);
			
			//store to database
			int bkgId = BookingDAO.storeBooking(book);
			
			// redirect user to booking detail page
			response.sendRedirect("TpackageServlet?action=view&id=" + bkgId);
			
		}catch (ParseException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	protected void deleteBooking(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get booking id
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// delete booking record based on the id
			BookingDAO.deleteBooking(id);
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		// redirect user to booking list page
		response.sendRedirect("BookingServlet?action=list");
		
	}
	
	
	protected void editBooking(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get booking id
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			// get booking record
			Booking book = BookingDAO.getBooking(id);
			
			// set attribute
			request.setAttribute("book", book);
			
			// forward to JSP
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("booking/booking_update.jsp");
			requestDispatcher.forward(request, response);
	
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
	}
	
	
	protected void updateBooking(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {		
			// get the form fields
			String custIdStr = request.getParameter("custId");
			String tpackageIdStr = request.getParameter("tpackageId");
			String bookingDateStr = request.getParameter("bookingDate");
			String adminIdStr = request.getParameter("adminId");
			
			int booking_id = Integer.parseInt(request.getParameter("booking_id"));
			
			// set to object
			Booking book = new Booking();
			int custId = -1;
			if (!custIdStr.equals(""))
				custId = Integer.parseInt(custIdStr);
			book.setCust_id(custId);
			
			
			int tpaId = -1;
			if (!tpackageIdStr.equals(""))
				tpaId = Integer.parseInt(tpackageIdStr);
			book.setTpackage_id(tpaId);
			
			
			Date bookingDate=new SimpleDateFormat("yyyy-MM-dd").parse(bookingDateStr);
			book.setBooking_date(bookingDate);
			
			
			int admId = -1;
			if (!adminIdStr.equals(""))
				admId = Integer.parseInt(adminIdStr);
			book.setAdmin_id(admId);
			
			//store to database
			int bkgId = BookingDAO.storeBooking(book);
			
			// redirect user to booking detail page
			response.sendRedirect("TpackageServlet?action=view&id=" + bkgId);
			
			//store to database
			boolean updateStatus = BookingDAO.updateBooking(book);
			
			// redirect user to booking detail page
			response.sendRedirect("BookingServlet?action=view&id=" + booking_id);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

}

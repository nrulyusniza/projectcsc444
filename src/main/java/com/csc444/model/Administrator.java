package com.csc444.model;

public class Administrator {
	
	// list of attributes
	private int admin_id;
	private String admin_firstname;
	private String admin_lastname;
	private String admin_password;
	private String admin_nric;
	private String admin_phoneno;
	private String admin_email;
	
	// default constructor
	public Administrator () {
		
	}
	
	public Administrator (int admin_id, String admin_firstname, String admin_lastname, 
			String admin_password, String admin_nric,  
			String admin_phoneno, String admin_email) {
		
		this.admin_id = admin_id;
		this.admin_firstname = admin_firstname;
		this.admin_lastname = admin_lastname;
		this.admin_password = admin_password;
		this.admin_nric = admin_nric;
		this.admin_phoneno = admin_phoneno;
		this.admin_email = admin_email;
	}
	
	// getter and setter
	public int getAdmin_id() {
		return admin_id;
	}

	public void setAdmin_id(int admin_id) {
		this.admin_id = admin_id;
	}

	public String getAdmin_firstname() {
		return admin_firstname;
	}

	public void setAdmin_firstname(String admin_firstname) {
		this.admin_firstname = admin_firstname;
	}

	public String getAdmin_lastname() {
		return admin_lastname;
	}

	public void setAdmin_lastname(String admin_lastname) {
		this.admin_lastname = admin_lastname;
	}

	public String getAdmin_password() {
		return admin_password;
	}

	public void setAdmin_password(String admin_password) {
		this.admin_password = admin_password;
	}

	public String getAdmin_nric() {
		return admin_nric;
	}

	public void setAdmin_nric(String admin_nric) {
		this.admin_nric = admin_nric;
	}

	public String getAdmin_phoneno() {
		return admin_phoneno;
	}

	public void setAdmin_phoneno(String admin_phoneno) {
		this.admin_phoneno = admin_phoneno;
	}

	public String getAdmin_email() {
		return admin_email;
	}

	public void setAdmin_email(String admin_email) {
		this.admin_email = admin_email;
	}

	// toString()
	@Override
	public String toString() {
		return "Administrator [admin_id=" + admin_id + ", admin_firstname=" + admin_firstname + ", admin_lastname="
				+ admin_lastname + ", admin_password=" + admin_password + ", admin_nric=" + admin_nric
				+ ", admin_phoneno=" + admin_phoneno + ", admin_email=" + admin_email + "]";
	}
	
	
} // end class Administrator

package com.csc444.model;

import java.util.Date;

public class Booking {
	
	// list of attributes
	private int booking_id;
	
	private int cust_id;	//FK
	private Customer customer;
	
	private int tpackage_id;	//FK
	private Tpackage tpackage;
	
	private Date booking_date;
	
	private int admin_id;	//FK
	private Administrator administrator;
	
	// default constructor
	public Booking () {
		
	}
	
	public Booking (int booking_id, int cust_id, int tpackage_id, 
			Date booking_date, int admin_id) {
		
		this.booking_id = booking_id;
		this.cust_id = cust_id;
		this.tpackage_id = tpackage_id;
		this.booking_date = booking_date;
		this.admin_id = admin_id;
	}

	// getter and setter
	public int getBooking_id() {
		return booking_id;
	}

	public void setBooking_id(int booking_id) {
		this.booking_id = booking_id;
	}

	public int getCust_id() {
		return cust_id;
	}

	public void setCust_id(int cust_id) {
		this.cust_id = cust_id;
	}

	public int getTpackage_id() {
		return tpackage_id;
	}

	public void setTpackage_id(int tpackage_id) {
		this.tpackage_id = tpackage_id;
	}

	public Date getBooking_date() {
		return booking_date;
	}

	public void setBooking_date(Date booking_date) {
		this.booking_date = booking_date;
	}

	public int getAdmin_id() {
		return admin_id;
	}

	public void setAdmin_id(int admin_id) {
		this.admin_id = admin_id;
	}

	// toString()
	@Override
	public String toString() {
		return "Booking [booking_id=" + booking_id + ", cust_id=" + cust_id + ", tpackage_id=" + tpackage_id
				+ ", booking_date=" + booking_date + ", admin_id=" + admin_id + "]";
	}
	
	// foreign key
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
	public Tpackage getTpackage() {
		return tpackage;
	}
	
	public void setTpackage(Tpackage tpackage) {
		this.tpackage = tpackage;
	}
	
	
	public Administrator getAdministrator() {
		return administrator;
	}
	
	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}

} // end class Booking

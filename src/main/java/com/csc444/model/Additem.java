package com.csc444.model;

public class Additem {
	
	// list of attributes
	private int additem_id;
	private String additem_name;
	private int additem_qty;
	private double additem_price;
	private double additem_disc;
	
	// default constructor
	public Additem () {
		
	}

	public Additem (int additem_id, String additem_name, int additem_qty, 
			double additem_price, double additem_disc) {
		
		this.additem_id = additem_id;
		this.additem_name = additem_name;
		this.additem_qty = additem_qty;
		this.additem_price = additem_price;
		this.additem_disc = additem_disc;
	}

	// getter and setter
	public int getAdditem_id() {
		return additem_id;
	}

	public void setAdditem_id(int additem_id) {
		this.additem_id = additem_id;
	}

	public String getAdditem_name() {
		return additem_name;
	}

	public void setAdditem_name(String additem_name) {
		this.additem_name = additem_name;
	}

	public int getAdditem_qty() {
		return additem_qty;
	}

	public void setAdditem_qty(int additem_qty) {
		this.additem_qty = additem_qty;
	}

	public double getAdditem_price() {
		return additem_price;
	}

	public void setAdditem_price(double additem_price) {
		this.additem_price = additem_price;
	}

	public double getAdditem_disc() {
		return additem_disc;
	}

	public void setAdditem_disc(double additem_disc) {
		this.additem_disc = additem_disc;
	}

	// toString()
	@Override
	public String toString() {
		return "Additem [additem_id=" + additem_id + ", additem_name=" + additem_name + ", additem_qty=" + additem_qty
				+ ", additem_price=" + additem_price + ", additem_disc=" + additem_disc + "]";
	}
	
} // end class Additem

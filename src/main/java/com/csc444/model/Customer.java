package com.csc444.model;

public class Customer {
	
	// list of attributes
	private int cust_id;
	private String cust_firstname;
	private String cust_lastname;
	private String cust_password;
	private String cust_nric;
	private String cust_address;
	private String cust_phoneno;
	private String cust_email;
	
	// default constructor
	public Customer () {
		
	}
	
	public Customer (int cust_id, String cust_firstname, String cust_lastname, 
			String cust_password, String cust_nric, String cust_address, 
			String cust_phoneno, String cust_email) {
		
		this.cust_id = cust_id;
		this.cust_firstname = cust_firstname;
		this.cust_lastname = cust_lastname;
		this.cust_password = cust_password;
		this.cust_nric = cust_nric;
		this.cust_address = cust_address;
		this.cust_phoneno = cust_phoneno;
		this.cust_email = cust_email;
	}
	
	// getter and setter
	public int getCust_id() {
		return cust_id;
	}

	public void setCust_id(int cust_id) {
		this.cust_id = cust_id;
	}

	public String getCust_firstname() {
		return cust_firstname;
	}

	public void setCust_firstname(String cust_firstname) {
		this.cust_firstname = cust_firstname;
	}

	public String getCust_lastname() {
		return cust_lastname;
	}

	public void setCust_lastname(String cust_lastname) {
		this.cust_lastname = cust_lastname;
	}

	public String getCust_password() {
		return cust_password;
	}

	public void setCust_password(String cust_password) {
		this.cust_password = cust_password;
	}

	public String getCust_nric() {
		return cust_nric;
	}

	public void setCust_nric(String cust_nric) {
		this.cust_nric = cust_nric;
	}

	public String getCust_address() {
		return cust_address;
	}

	public void setCust_address(String cust_address) {
		this.cust_address = cust_address;
	}

	public String getCust_phoneno() {
		return cust_phoneno;
	}

	public void setCust_phoneno(String cust_phoneno) {
		this.cust_phoneno = cust_phoneno;
	}

	public String getCust_email() {
		return cust_email;
	}

	public void setCust_email(String cust_email) {
		this.cust_email = cust_email;
	}

	// toString()
	@Override
	public String toString() {
		return "Customer [cust_id=" + cust_id + ", cust_firstname=" + cust_firstname + ", cust_lastname="
				+ cust_lastname + ", cust_password=" + cust_password + ", cust_nric=" + cust_nric + ", cust_address="
				+ cust_address + ", cust_phoneno=" + cust_phoneno + ", cust_email=" + cust_email + "]";
	}
	
} // end class Customer

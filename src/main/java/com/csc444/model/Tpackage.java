package com.csc444.model;

public class Tpackage {
	
	// list of attributes
	private int tpackage_id;
	private String tpackage_name;
	private int tpackage_qty;
	private double tpackage_price;
	private double tpackage_disc;
	
	// default constructor
	public Tpackage () {
		
	}
	
	public Tpackage (int tpackage_id, String tpackage_name, 
			int tpackage_qty, double tpackage_price, double tpackage_disc) {
		
		this.tpackage_id = tpackage_id;
		this.tpackage_name = tpackage_name;
		this.tpackage_qty = tpackage_qty;
		this.tpackage_price = tpackage_price;
		this.tpackage_disc = tpackage_disc;
	}
	
	// getter and setter
	public int getTpackage_id() {
		return tpackage_id;
	}

	public void setTpackage_id(int tpackage_id) {
		this.tpackage_id = tpackage_id;
	}

	public String getTpackage_name() {
		return tpackage_name;
	}

	public void setTpackage_name(String tpackage_name) {
		this.tpackage_name = tpackage_name;
	}

	public int getTpackage_qty() {
		return tpackage_qty;
	}

	public void setTpackage_qty(int tpackage_qty) {
		this.tpackage_qty = tpackage_qty;
	}

	public double getTpackage_price() {
		return tpackage_price;
	}

	public void setTpackage_price(double tpackage_price) {
		this.tpackage_price = tpackage_price;
	}

	public double getTpackage_disc() {
		return tpackage_disc;
	}

	public void setTpackage_disc(double tpackage_disc) {
		this.tpackage_disc = tpackage_disc;
	}

	// toString()
	@Override
	public String toString() {
		return "Tpackage [tpackage_id=" + tpackage_id + ", tpackage_name=" + tpackage_name + ", tpackage_qty="
				+ tpackage_qty + ", tpackage_price=" + tpackage_price + ", tpackage_disc=" + tpackage_disc + "]";
	}
		

}

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Details ${tpack.tpackage_name }</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">

<body>
<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
	<i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  	<div class="w3-center">
  		<h1 class="w3-xxxlarge w3-animate-bottom">PANDA WORLD SDN BHD</h1>
	    <div class="w3-padding-32">
	    
	    </div>
  	</div>
</header>
</head>

<%-- Display single Package info --%>
<body>
	<div class="w3-container">
	<hr>
		<div class="w3-center">
	    <h2>Details ${tpack.tpackage_name }</h2>
  		</div>
	<div class="w3-responsive w3-card-4">
	<table class="w3-table w3-striped w3-bordered">
		<tr>
			<td>Package  ID:</td>
			<td><b>${tpack.tpackage_id }</b></td>
		</tr>
		<tr>
			<td>Package  Name:</td>
			<td><b>${tpack.tpackage_name }</b></td>
		</tr>
		<tr>
			<td>Package Quantity:</td>
			<td><b>${tpack.tpackage_qty }</b></td>
		</tr>
		<tr>
			<td>Package Price:</td>
			<td><b>${tpack.tpackage_price }</b></td>
		</tr>
		<tr>
			<td>Package Discount:</td>
			<td><b>${tpack.tpackage_disc }</b></td>
		</tr>
	</table>
	</div>
	<br>
	<a href="TpackageServlet?action=list">Back to List of Package</a>
	</div>
</body>
</html>
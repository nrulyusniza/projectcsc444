<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert new Package record</title>
</head>
<%-- Display insert Package form --%>
<body>
	<h1>Insert new Package record</h1>
	<form action="TpackageServlet?action=store" method="POST">
	<table>
		<tr>
			<td>Package Name:</td>
			<td><input type="text" name="tpackagename" required/></td>
		</tr>
		<tr>
			<td>Package Quantity:</td>
			<td><input type="number" name="tpackageqty" required/></td>
		</tr>
		<tr>
			<td>Package Price:</td>
			<td><input type="text" name="tpackageprice" required/></td>
		</tr>
		<tr>
			<td>Package Discount:</td>
			<td><input type="text" name="tpackagedisc"/></td>
		</tr>
	</table>
	</form>
	<a href="TpackageServlet?action=list">Back to List of Package</a>
</body>
</html>
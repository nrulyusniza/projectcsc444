<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update ${tpack.tpackage_id }</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">

<body>
<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
	<i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  	<div class="w3-center">
  		<h1 class="w3-xxxlarge w3-animate-bottom">PANDA WORLD SDN BHD</h1>
	    <div class="w3-padding-32">
	    
	    </div>
  	</div>
</header>
</head>

<%-- Display edit Package form --%>
<body>
	<div class="w3-container">
	<hr>
		<div class="w3-center">
	    <h2>Update Package Record [ID: ${tpack.tpackage_id }]</h2>
  		</div>
	<div class="w3-responsive w3-card-4">
	<form action="TpackageServlet?action=update" method="POST">
	<table class="w3-table w3-striped w3-bordered">
		<tr>
			<td>Package Name:</td>
			<td><input type="text" name="tpackage_name" value="${tpack.tpackage_name }" required/></td>
		</tr>
		<tr>
			<td>Package Quantity:</td>
			<td><input type="number" name="tpackage_qty" value="${tpack.tpackage_qty }"/></td>
		</tr>
		<tr>
			<td>Package Price:</td>
			<td><input type="text" name="tpackage_price" value="${tpack.tpackage_price }" required/></td>
		</tr>
		<tr>
			<td>Package Discount:</td>
			<td><input type="text" name="tpackage_disc" value="${tpack.tpackage_disc }" required/></td>
		</tr>
		<tr>
			<td></td>
			<td align="right"><input type="submit" value="SUBMIT"/></td>
		</tr>
	</table>
		<input type="hidden" name="tpackage_id" value="${tpack.tpackage_id }"/>
	</form>
	</div>
	<br>
	<a href="TpackageServlet?action=list">Back to List of Package</a>
	</div>
</body>
</html>
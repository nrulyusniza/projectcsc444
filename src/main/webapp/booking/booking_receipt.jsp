<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Site Metas -->
    <title>Receipt Generation</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/cssbooking/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="../css/cssbooking/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../css/cssbooking/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/cssbooking/custom.css">
    <!-- Receipt link -->
    <link rel="stylesheet" href="../css/css/receipt.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<!-- Nav -->                
<nav class="navbar navbar-inverse bg-inverse fixed-top bg-faded">
    <div class="row">
        <div class="col">
          </div><li><a class="nav-item active"><a class="nav-link" href="../booking/booking_index.jsp">Booking</a></li>
          <a class="nav-link" href="../CustomerServlet?action=logout">Logout</a></li>
                    
    </div>
    
</nav>


<div id="invoice-POS">
    
<center id="top">
      <div class="logo"></div>
      <div class="info"> 
        <h2>Panda World SDN BHD</h2>
      </div>
      <!--End Info-->
    </center>
    <!--End InvoiceTop-->
    
    <div id="mid">
      <div class="info">
        <h2>Contact Info</h2>
        <p> 
            First Name : Izzat </br>
            Last Name  : Aidan </br>
            Email      : izzat@gmail.com</br>
            Phone No   : 0123456789</br>
        </p>
      </div>
    </div><!--End Invoice Mid-->
    
    <div id="bot">

					<div id="table">
						<table>
							<tr class="tabletitle">
								<td class="item"><h2>Item</h2></td>
								<td class="Hours"><h2>Qty</h2></td>
								<td class="Rate"><h2>Sub Total</h2></td>
							</tr>

							<tr class="service">
								<td class="tableitem"><p class="itemtext">Standard</p></td>
								<td class="tableitem"><p class="itemtext">1</p></td>
								<td class="tableitem"><p class="itemtext">RM 450.00</p></td>
							</tr>

							<tr class="service">
								<td class="tableitem"><p class="itemtext">Plastic Chair</p></td>
								<td class="tableitem"><p class="itemtext">60</p></td>
								<td class="tableitem"><p class="itemtext">RM 60.00</p></td>
							</tr>

							<tr class="service">
								<td class="tableitem"><p class="itemtext">Bride Groom Table</p></td>
								<td class="tableitem"><p class="itemtext">10</p></td>
								<td class="tableitem"><p class="itemtext">RM 200.00</p></td>
							</tr>

							<tr class="service">
								<td class="tableitem"><p class="itemtext">Stand Fan</p></td>
								<td class="tableitem"><p class="itemtext">10</p></td>
								<td class="tableitem"><p class="itemtext">RM 250.00</p></td>
							</tr>

							<tr class="service">
								<td class="tableitem"><p class="itemtext">Round Table</p></td>
								<td class="tableitem"><p class="itemtext">10</p></td>
								<td class="tableitem"><p class="itemtext">RM 150.00</p></td>
							</tr>
							
							<tr class="service">
								<td class="tableitem"><p class="itemtext">Canopy Arabian</p></td>
								<td class="tableitem"><p class="itemtext">6</p></td>
								<td class="tableitem"><p class="itemtext">RM 1200.00</p></td>
							</tr>

							<tr class="tabletitle">
								<td></td>
								<td class="Rate"><h2>Total</h2></td>
								<td class="payment"><h2>RM 2,310.00</h2></td>
							</tr>

						</table>
					</div><!--End Table-->

					<div id="legalcopy">
						<p class="legal"><strong>Thank you for your business!</strong>  Payment is expected within 30 days; please process this invoice within that time. 
						</p>
					</div>

				</div><!--End InvoiceBot-->
  </div><!--End Invoice-->
</body>
</html>
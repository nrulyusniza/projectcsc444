<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Site Metas -->
    <title>Customer Booking</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/cssbooking/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="../css/cssbooking/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../css/cssbooking/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../css/cssbooking/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Start Main Top -->
    <header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                  <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#cart">Cart (<span class="total-count"></span>)</button><button class="clear-cart btn btn-danger">Clear Cart</button></div>
    
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand" href="index.html"><img src="images/logo.png" class="logo" alt=""></a>
                </div>
                <!-- End Header Navigation -->

     
        <!-- End Navigation -->
    </header>
    <!-- End Main Top -->
        
    <!-- Nav -->                
<nav class="navbar navbar-inverse bg-inverse fixed-top bg-faded">
    <div class="row">
        <div class="col">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cart">Cart (<span class="total-count"></span>)</button>
          <button class="clear-cart btn btn-danger">Clear Cart</button></div>
          <li class="nav-item active"><a class="nav-link" href="../booking/booking_index.jsp">Booking</a></li>
          <a class="nav-link" href="../CustomerServlet?action=logout">Logout</a></li>
                    
    </div>
    
</nav>

    <!-- Start Products  -->
    <div class="products-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Featured Package</h1>
                    </div>
                </div>
            </div>
            
            <!-- Main -->
<div class="container">
		<div class="row">
			<div class="col">
				        <div class="card" style="width: 20rem;">
				  <img class="card-img-top" src="../css/images/bg-06.jpg" alt="Card image cap">
				  <div class="card-block">
				    <h4 class="card-title">Economy</h4>
				    <p class="card-text">Price: RM 200.00/PACKAGE</p>
				    <a href="#" data-name="Economy" data-price="200.00" class="add-to-cart btn btn-primary">Add to cart</a>
				  </div>
				</div>
			</div>
		<div class="col">
			 <div class="card" style="width: 20rem;">
				  <img class="card-img-top" src="../css/images/bg-05.jpg" alt="Card image cap">
				  <div class="card-block">
				    <h4 class="card-title">Standard</h4>
				    <p class="card-text">Price: RM 450.00/PACKAGE</p>
				    <a href="#" data-name="Standard" data-price="450.00" class="add-to-cart btn btn-primary">Add to cart</a>
				  </div>
			</div>
		</div>
		<div class="col">
			<div class="card" style="width: 20rem;">
				  <img class="card-img-top" src="../css/images/bg-07.jpg" alt="Card image cap">
				  <div class="card-block">
				    <h4 class="card-title">Platinum</h4>
				    <p class="card-text">Price: RM 600.00/PACKAGE</p>
				    <a href="#" data-name="Platinum" data-price="600.00" class="add-to-cart btn btn-primary">Add to cart</a>
				  </div>
			</div>
		</div>
		            <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Additional item</h1>
                    </div>
                	</div>
		<div class="col">
				        <div class="card" style="width: 20rem;">
				  <img class="card-img-top" src="../css/images/bg-08.jpg" alt="Card image cap">
				  <div class="card-block">
				    <h4 class="card-title">Canopy Arabian</h4>
				    <p class="card-text">Price: RM 200.00/unit quantity</p>
				    <a href="#" data-name="Canopy Arabian" data-price="200.00" class="add-to-cart btn btn-primary">Add to cart</a>
				  </div>
				</div>
			</div>
		<div class="col">
			 <div class="card" style="width: 20rem;">
				  <img class="card-img-top" src="../css/images/bg-11.jpg" alt="Card image cap">
				  <div class="card-block">
				    <h4 class="card-title">Plastic chair</h4>
				    <p class="card-text">Price: RM 1.00/unit quantity</p>
				    <a href="#" data-name="Plastic Chair" data-price="1.00" class="add-to-cart btn btn-primary">Add to cart</a>
				  </div>
			</div>
		</div>
		<div class="col">
			<div class="card" style="width: 20rem;">
				  <img class="card-img-top" src="../css/images/bg-10.jpg" alt="Card image cap">
				  <div class="card-block">
				    <h4 class="card-title">Bride Groom Table</h4>
				    <p class="card-text">Price: RM 20.00/unit quantity</p>
				    <a href="#" data-name="Bride Groom Table" data-price="20.00" class="add-to-cart btn btn-primary">Add to cart</a>
				  </div>
			</div>
		</div>
		<div class="col">
				        <div class="card" style="width: 20rem;">
				  <img class="card-img-top" src="../css/images/bg-09.jpg" alt="Card image cap">
				  <div class="card-block">
				    <h4 class="card-title">Stand Fan</h4>
				    <p class="card-text">Price: RM 25.00/unit quantity</p>
				    <a href="#" data-name="Stand Fan" data-price="25.00" class="add-to-cart btn btn-primary">Add to cart</a>
				  </div>
				</div>
			</div>
		<div class="col">
			 <div class="card" style="width: 20rem;">
				  <img class="card-img-top" src="../css/images/bg-12.jpg" alt="Card image cap">
				  <div class="card-block">
				    <h4 class="card-title">Round Table</h4>
				    <p class="card-text">Price: RM 15.00/unit quantity</p>
				    <a href="#" data-name="Round Table" data-price="15.00" class="add-to-cart btn btn-primary">Add to cart</a>
				  </div>
			</div>
		</div>
		
			
			      </div>
      


<!-- Modal important -->
<div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cart</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="show-cart table">
          
        </table>
        <div>Total price: RM <span class="total-cart"></span></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="booking_receipt.jsp" class="btn btn-primary">Pay now</a>
      </div>
    </div>
  </div>
</div> 

  



    <a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

    <!-- ALL JS FILES -->
    <script src="../css/jsbooking/jquery-3.2.1.min.js"></script>
    <script src="../css/jsbooking/popper.min.js"></script>
    <script src="../css/jsbooking/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="../css/jsbooking/jquery.superslides.min.js"></script>
    <script src="../css/jsbooking/bootstrap-select.js"></script>
    <script src="../css/jsbooking/inewsticker.js"></script>
    <script src="../css/jsbooking/bootsnav.js."></script>
    <script src="../css/jsbooking/images-loded.min.js"></script>
    <script src="../css/jsbooking/isotope.min.js"></script>
    <script src="../css/jsbooking/owl.carousel.min.js"></script>
    <script src="../css/jsbooking/baguetteBox.min.js"></script>
    <script src="../css/jsbooking/form-validator.min.js"></script>
    <script src="../css/jsbooking/contact-form-script.js"></script>
    <script src="../css/jsbooking/custom.js"></script>
</body>
<script>
// ************************************************
// Shopping Cart API
// ************************************************

var shoppingCart = (function() {
  // =============================
  // Private methods and propeties
  // =============================
  cart = [];
  
  // Constructor
  function Item(name, price, count) {
    this.name = name;
    this.price = price;
    this.count = count;
  }
  
  // Save cart
  function saveCart() {
    sessionStorage.setItem('shoppingCart', JSON.stringify(cart));
  }
  
    // Load cart
  function loadCart() {
    cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
  }
  if (sessionStorage.getItem("shoppingCart") != null) {
    loadCart();
  }
  

  // =============================
  // Public methods and propeties
  // =============================
  var obj = {};
  
  // Add to cart
  obj.addItemToCart = function(name, price, count) {
    for(var item in cart) {
      if(cart[item].name === name) {
        cart[item].count ++;
        saveCart();
        return;
      }
    }
    var item = new Item(name, price, count);
    cart.push(item);
    saveCart();
  }
  // Set count from item
  obj.setCountForItem = function(name, count) {
    for(var i in cart) {
      if (cart[i].name === name) {
        cart[i].count = count;
        break;
      }
    }
  };
  // Remove item from cart
  obj.removeItemFromCart = function(name) {
      for(var item in cart) {
        if(cart[item].name === name) {
          cart[item].count --;
          if(cart[item].count === 0) {
            cart.splice(item, 1);
          }
          break;
        }
    }
    saveCart();
  }

  // Remove all items from cart
  obj.removeItemFromCartAll = function(name) {
    for(var item in cart) {
      if(cart[item].name === name) {
        cart.splice(item, 1);
        break;
      }
    }
    saveCart();
  }

  // Clear cart
  obj.clearCart = function() {
    cart = [];
    saveCart();
  }

  // Count cart 
  obj.totalCount = function() {
    var totalCount = 0;
    for(var item in cart) {
      totalCount += cart[item].count;
    }
    return totalCount;
  }

  // Total cart
  obj.totalCart = function() {
    var totalCart = 0;
    for(var item in cart) {
      totalCart += cart[item].price * cart[item].count;
    }
    return Number(totalCart.toFixed(2));
  }

  // List cart
  obj.listCart = function() {
    var cartCopy = [];
    for(i in cart) {
      item = cart[i];
      itemCopy = {};
      for(p in item) {
        itemCopy[p] = item[p];

      }
      itemCopy.total = Number(item.price * item.count).toFixed(2);
      cartCopy.push(itemCopy)
    }
    return cartCopy;
  }

  // cart : Array
  // Item : Object/Class
  // addItemToCart : Function
  // removeItemFromCart : Function
  // removeItemFromCartAll : Function
  // clearCart : Function
  // countCart : Function
  // totalCart : Function
  // listCart : Function
  // saveCart : Function
  // loadCart : Function
  return obj;
})();


// *****************************************
// Triggers / Events
// ***************************************** 
// Add item
$('.add-to-cart').click(function(event) {
  event.preventDefault();
  var name = $(this).data('name');
  var price = Number($(this).data('price'));
  shoppingCart.addItemToCart(name, price, 1);
  displayCart();
});

// Clear items
$('.clear-cart').click(function() {
  shoppingCart.clearCart();
  displayCart();
});


function displayCart() {
  var cartArray = shoppingCart.listCart();
  var output = "";
  for(var i in cartArray) {
    output += "<tr>"
      + "<td>" + cartArray[i].name + "</td>" 
      + "<td>(" + cartArray[i].price + ")</td>"
      + "<td><div class='input-group'><button class='minus-item input-group-addon btn btn-primary' data-name=" + cartArray[i].name + ">-</button>"
      + "<input type='number' class='item-count form-control' data-name='" + cartArray[i].name + "' value='" + cartArray[i].count + "'>"
      + "<button class='plus-item btn btn-primary input-group-addon' data-name=" + cartArray[i].name + ">+</button></div></td>"
      + "<td><button class='delete-item btn btn-danger' data-name=" + cartArray[i].name + ">X</button></td>"
      + " = " 
      + "<td>" + cartArray[i].total + "</td>" 
      +  "</tr>";
  }
  $('.show-cart').html(output);
  $('.total-cart').html(shoppingCart.totalCart());
  $('.total-count').html(shoppingCart.totalCount());
}

// Delete item button

$('.show-cart').on("click", ".delete-item", function(event) {
  var name = $(this).data('name')
  shoppingCart.removeItemFromCartAll(name);
  displayCart();
})


// -1
$('.show-cart').on("click", ".minus-item", function(event) {
  var name = $(this).data('name')
  shoppingCart.removeItemFromCart(name);
  displayCart();
})
// +1
$('.show-cart').on("click", ".plus-item", function(event) {
  var name = $(this).data('name')
  shoppingCart.addItemToCart(name);
  displayCart();
})

// Item count input
$('.show-cart').on("change", ".item-count", function(event) {
   var name = $(this).data('name');
   var count = Number($(this).val());
  shoppingCart.setCountForItem(name, count);
  displayCart();
});

displayCart();

</script>

</html>

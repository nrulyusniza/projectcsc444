<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Panda World Sdn Bhd Booking System</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<body>

<!-- Side Navigation -->
<nav class="w3-sidebar w3-bar-block w3-card w3-animate-left w3-center" style="display:none" id="mySidebar">
  <h1 class="w3-xxxlarge w3-text-theme">Side Navigation</h1>
  <button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
  <a href="AdministratorServlet?action=list" class= "w3-bar-item w3-button">Admin list</a>
  <a href="AdministratorServlet?action=create" class= "w3-bar-item w3-button">Insert new Admin</a>
  <a href="CustomerServlet?action=create" class= "w3-bar-item w3-button">Insert new Customer</a>
  <a href="TpackageServlet?action=list" class= "w3-bar-item w3-button">Tents Package</a>
  <a href="AdditemServlet?action=list" class="w3-bar-item w3-button">Additional Items</a>
  <a href="#" class="w3-bar-item w3-button">Link 2</a>
  <a href="#" class="w3-bar-item w3-button">Link 3</a>
  <a href="#" class="w3-bar-item w3-button">Link 4</a>
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-center">
  <h1 class="w3-xxxlarge w3-animate-bottom">PANDA WORLD SDN BHD</h1>
    <div class="w3-padding-32">
    
    </div>
  </div>
</header>
<meta charset="ISO-8859-1">
<title>${admin.admin_firstname } ${admin.admin_lastname }</title>
</head>
<%-- Display single Admin info --%>
<body>
	<table>
		<tr>
			<td>Admin ID:</td>
			<td><b>${admin.admin_id }</b></td>
		</tr>
		<tr>
			<td>First Name:</td>
			<td><b>${admin.admin_firstname }</b></td>
		</tr>
		<tr>
			<td>Last Name:</td>
			<td><b>${admin.admin_lastname }</b></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><b>${admin.admin_password }</b></td>
		</tr>
		<tr>
			<td>Identity ID:</td>
			<td><b>${admin.admin_nric }</b></td>
		</tr>
		<tr>
			<td>Phone Number:</td>
			<td><b>${admin.admin_phoneno }</b></td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><b>${admin.admin_email }</b></td>
		</tr>
	</table>
	<a href="AdministratorServlet?action=list">Back to List of Administrator</a>
</body>
</html>
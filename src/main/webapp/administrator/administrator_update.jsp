<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Panda World Sdn Bhd Booking System</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<body>

<!-- Side Navigation -->
<nav class="w3-sidebar w3-bar-block w3-card w3-animate-left w3-center" style="display:none" id="mySidebar">
  <h1 class="w3-xxxlarge w3-text-theme">Side Navigation</h1>
  <button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
  <a href="../Administrator_index" class= "w3-bar-item w3-button">Admin Home Page</a>
  <a href="AdministratorServlet?action=list" class= "w3-bar-item w3-button">Admin list</a>
  <a href="AdministratorServlet?action=create" class= "w3-bar-item w3-button">Insert new Admin</a>
  <a href="CustomerServlet?action=create" class= "w3-bar-item w3-button">Insert new Customer</a>
  <a href="TpackageServlet?action=list" class= "w3-bar-item w3-button">Tents Package</a>
  <a href="AdditemServlet?action=list" class="w3-bar-item w3-button">Additional Items</a>
  <a href="#" class="w3-bar-item w3-button">Link 2</a>
  <a href="#" class="w3-bar-item w3-button">Link 3</a>
  <a href="#" class="w3-bar-item w3-button">Link 4</a>
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-center">
  <h1 class="w3-xxxlarge w3-animate-bottom">PANDA WORLD SDN BHD</h1>
    <div class="w3-padding-32">
    
    </div>
  </div>
</header>
</head>
<body>
	<h1>Update Admin record [ID: ${admin.admin_id }]</h1>
	<form action="AdministratorServlet?action=update" method="POST">
	<table>
		<tr>
			<td>First Name:</td>
			<td><input type="text" name="admin_firstname" value="${admin.admin_firstname }" required/></td>
		</tr>
		<tr>
			<td>Last Name:</td>
			<td><input type="text" name="admin_lastname" value="${admin.admin_lastname }"/></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><input type="password" name="admin_password" value="${admin.admin_password }" required/></td>
		</tr>
		<tr>
			<td>Identity ID:</td>
			<td><input type="text" name="admin_nric" value="${admin.admin_nric }" required/></td>
		</tr>
		<tr>
			<td>Phone Number:</td>
			<td><input type="text" name="admin_phoneno" value="${admin.admin_phoneno }" required/></td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><input type="email" name="admin_email" value="${admin.admin_email }" required/></td>
		</tr>
		<tr>
			<td></td>
			<td align="right"><input type="submit" value="SUBMIT"/></td>
		</tr>
	</table>
		<input type="hidden" name="admin_id" value="${admin.admin_id }"/>
	</form>
	<a href="AdministratorServlet?action=list">Back to List of Admin</a>
</body>
</html>
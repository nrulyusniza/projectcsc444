<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="css/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<div class="wrapper row1">
  <header id="header" class="hoc clear">
    <div id="logo" class="fl_left"> 
      <h1 class="logoname"><a href="index.html">Panda<span>W</span>orld Sdn Bhd</a></h1>
    </div>
    <nav id="mainav" class="fl_right"> 
      <ul class="clear">
        <li><a href="CustomerServlet?action=create">New Customer Registration</a></li>
        <li><a href="index.html">Home</a></li>
        
        </ul>
        </nav>
  </header>
  </div>

	<form action="CustomerServlet?action=store" method="POST">
	<table>
		<tr>
			<td>First Name:</td>
			<td><input type="text" name="cust_firstname" required/></td>
		</tr>
		<tr>
			<td>Last Name:</td>
			<td><input type="text" name="cust_lastname" /></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><input type="password" name="cust_password" required/></td>
		</tr>
		<tr>
			<td>Identity ID:</td>
			<td><input type="text" name="cust_nric" required/></td>
		</tr>
		<tr>
			<td>Address:</td>
			<td><input type="text" name="cust_address" /></td>
		</tr>
		<tr>
			<td>Phone Number:</td>
			<td><input type="text" name="cust_phoneno" required/></td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><input type="email" name="cust_email" required/></td>
		</tr>
		<tr>
			<td></td>
			<td align="right"><input type="submit" value="SUBMIT"/></td>
		</tr>
	</table>
	</form>
</body>
</html>
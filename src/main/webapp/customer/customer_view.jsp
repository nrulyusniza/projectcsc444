<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="css/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<div class="wrapper row1">
  <header id="header" class="hoc clear">
    <div id="logo" class="fl_left"> 
      <h1 class="logoname"><a href="index.html">Panda<span>W</span>orld Sdn Bhd</a></h1>
    </div>
    <nav id="mainav" class="fl_right"> 
      <ul class="clear">
        <li><a href="CustomerServlet?action=create">New Customer Registration</a></li>
        <li><a href="index.html">Home</a></li>
        
        </ul>
        </nav>
  </header>
  </div>
<body>
	<table>
		<tr>
			<td>Customer ID:</td>
			<td><b>${cust.cust_id }</b></td>
		</tr>
		<tr>
			<td>First Name:</td>
			<td><b>${cust.cust_firstname }</b></td>
		</tr>
		<tr>
			<td>Last Name:</td>
			<td><b>${cust.cust_lastname }</b></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><b>${cust.cust_password }</b></td>
		</tr>
		<tr>
			<td>Identity ID:</td>
			<td><b>${cust.cust_nric }</b></td>
		</tr>
		<tr>
			<td>Address:</td>
			<td><b>${cust.cust_address }</b></td>
		</tr>
		<tr>
			<td>Phone Number:</td>
			<td><b>${cust.cust_phoneno }</b></td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><b>${cust.cust_email }</b></td>
		</tr>
	</table>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert new Additional Item record</title>
</head>
<%-- Display insert Additional Item form --%>
<body>
	<h1>Insert new Additional Item record</h1>
	<form action="AdditemServlet?action=store" method="POST">
	<table>
		<tr>
			<td>Additional Item Name:</td>
			<td><input type="text" name="additemname" required/></td>
		</tr>
		<tr>
			<td>Additional Item Quantity:</td>
			<td><input type="number" name="additemqty" required/></td>
		</tr>
		<tr>
			<td>Additional Item Price:</td>
			<td><input type="text" name="additemprice" required/></td>
		</tr>
		<tr>
			<td>Additional Item Discount:</td>
			<td><input type="text" name="additemdisc"/></td>
		</tr>
	</table>
	</form>
	<a href="AdditemServlet?action=list">Back to List of Additional Item</a>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Panda World Sdn Bhd Booking System</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
<body>

<!-- Side Navigation -->
<nav class="w3-sidebar w3-bar-block w3-card w3-animate-left w3-center" style="display:none" id="mySidebar">
  <h1 class="w3-xxxlarge w3-text-theme">Navigation</h1>
  <button class="w3-bar-item w3-button" onclick="w3_close()">Close <i class="fa fa-remove"></i></button>
  <a href="#" class="w3-bar-item w3-button">Home</a>
  <a href="TpackageServlet?action=list" class="w3-bar-item w3-button">Tents Package</a>
  <a href="AdditemServlet?action=list" class="w3-bar-item w3-button">Additional Item</a>
  <a href="#" class="w3-bar-item w3-button">Booking</a>
  <a href="#" class="w3-bar-item w3-button">Logout</a>
</nav>

<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader">
  <i onclick="w3_open()" class="fa fa-bars w3-xlarge w3-button w3-theme"></i> 
  <div class="w3-center">
  <h1 class="w3-xxxlarge w3-animate-bottom">PANDA WORLD SDN BHD</h1>
    <div class="w3-padding-32">
    
    </div>
  </div>
</header>
</head>

<%-- Display Additional Items for admin--%>
<body>
<div class="w3-container">
	<hr>
	<div class="w3-center">
		<h2>List of Additional Item</h2>
		<p w3-class="w3-large"></p>
	</div>
<div class="w3-responsive w3-card-4">
<table class="w3-table w3-striped w3-bordered">
<thead>
	<tr>
		<td>Additional Item ID</td>
		<td>Additional Item</td>
		<td>Price</td>
		<td>Action</td>
	</tr>
</thead>
<tbody>
	<c:forEach var="addit" items="${additem}">
	<tr>
		<td>${addit.additem_id}</td>
		<td>${addit.additem_name}</td>
		<td>RM ${addit.additem_price}</td>
		<td>
			<a href="AdditemServlet?action=view&id=${addit.additem_id}">View</a>&nbsp;
			<a href="AdditemServlet?action=edit&id=${addit.additem_id}">Update</a>
		</td>
	</tr>
	</c:forEach>
</tbody>
</table>
</div>

<script>
// Side navigation
function w3_open() {
  var x = document.getElementById("mySidebar");
  x.style.width = "100%";
  x.style.fontSize = "40px";
  x.style.paddingTop = "10%";
  x.style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}
</script>

</body>
</html>